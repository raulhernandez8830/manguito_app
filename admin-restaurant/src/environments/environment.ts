// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // urlapi: 'http://127.0.0.1:8000/',
  // urlproducts: 'http://127.0.0.1:8000/products/',
  // urlcategorias: 'http://127.0.0.1:8000/categorias/'

  //urlapi: 'http://127.0.0.1:8000/',
  tienda: 1,
  urlapi: 'https://api.appstgs.com/',
  urlproducts: 'https://api.appstgs.com/imagenes_server/',
  urlcategorias: 'https://tgsserver.com/El_Manguito/rest_manguito/public/categorias/',

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
