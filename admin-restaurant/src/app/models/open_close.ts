export class OpenClose {
  id: number;
  day: string;
  to: string;
  from: string;
  store_id: number;
}
