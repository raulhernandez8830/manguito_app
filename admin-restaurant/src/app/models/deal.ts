export class Deal {
  id: number;
  descripcion: string;
  product_id: number;
  descuento: number;
  inicio: string;
  fin: string;
}
