export class Category {
    id: number;
    nombre: string;
    descripcion: string;
    store_id: number;
    user_id: number;
    imagen: string;
}
