export class Product {
  id: number;
  image: string;
  barcode: string;
  nombre: string;
  descripcion: string;
  precio: number;
  categoria_id: number;
  user_id: number;
  store_id: number;
  created_at: string;
  estado: string;
  precio_oferta: number;
  tipo_menu: number;
  is_menu: boolean;
  dia: string;
  always_available: boolean;
}
