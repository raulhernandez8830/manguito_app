import { UserStore } from './user_store';
import { ProfileSocial } from './profile_social';
import { UserFirebase } from './user_firebase';
export class UserSocial {
    additionalUserInfo?: firebase.auth.AdditionalUserInfo | null;
    credential: firebase.auth.AuthCredential | null;
    operationType?: string | null;
    user: firebase.User | null;
}
