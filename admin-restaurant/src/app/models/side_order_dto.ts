export class SideOrderDTO{
  id: number;
  side_order: string;
  caracteristica: string;
  precio: number;
  tipo_side_order: number;
  user_id: number;
  store_id: number;
  estado: boolean;
  isChecked = true;
}
