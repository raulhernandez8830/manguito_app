export class TipoMenu {
  id: number;
  nombre: string;
  descripcion: string;
  store_id: number;
  user_id: number;
  estado: number;
}
