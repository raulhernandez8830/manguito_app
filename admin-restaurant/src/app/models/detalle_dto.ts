import { Product } from "./product";

export class DetalleDTO {
  cantidad: number;
  comentario: string;
  product: Product;
}
