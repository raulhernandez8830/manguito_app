export class TipoSideOrder {
  id: number;
  nombre: string;
  user_id: number;
  store_id: number;
  estado: boolean;
}
