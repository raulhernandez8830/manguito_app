import { SideOrderDetail } from './side_order_detail';
export class SideOrder {
  id: number;
  nombre: string;
  caracteristica: string;
  precio: number;
  tipo_side_order: number;
  user_id: number;
  store_id: number;
  estado: boolean;
  isChecked = true;

}
