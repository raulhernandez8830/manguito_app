import { SideOrderDTO } from './side_order_dto';
import { SideOrder } from './side_order';
import { DetallePedido } from './detalle_pedido';
export class Pedido{
  id:number;
  client_id: number;
  store_id: number;
  estado_id: boolean;
  fecha_creacion: string;
  opcion_delivery: number;
  opcion_pago: number;
  direccion_envio: string;
  direccion_fact: string;
  zipcode: string;
  postcode: string;
  city: string;
  state: string;
  total: number;
  total_descuento: number;
  user_id: number;
  detalle_pedido: DetallePedido[];
  side_orders: SideOrder[];


}
