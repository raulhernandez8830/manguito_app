import { Pipe, PipeTransform } from '@angular/core';
import { TipoMenu } from '../models/tipo_menu';

@Pipe({
  name: 'searchtipomenu'
})
export class SearchtipomenuPipe implements PipeTransform {

  transform(arreglo: TipoMenu[], texto: string = ''): any {
    if(texto === '') {
      return arreglo;
    }

    if(!arreglo) {
      return arreglo
    }

    return arreglo.filter(
      item => JSON.stringify(item).toLocaleLowerCase().includes(texto)
    );
  }

}
