import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchproductsPipe } from './searchproducts.pipe';
import { SearchcategoriesPipe } from './searchcategories.pipe';
import { SearchpedidoPipe } from './searchpedido.pipe';
import { SearchtipomenuPipe } from './searchtipomenu.pipe';
import { SearchsideorderPipe } from './searchsideorder.pipe';



@NgModule({
  declarations: [SearchproductsPipe,
    SearchcategoriesPipe,
    SearchpedidoPipe,
    SearchtipomenuPipe,
    SearchsideorderPipe],
  imports: [
    CommonModule
  ],
  exports: [
    SearchcategoriesPipe,
    SearchproductsPipe,
    SearchpedidoPipe,
    SearchtipomenuPipe,
    SearchsideorderPipe
  ]
})
export class PipesModule { }
