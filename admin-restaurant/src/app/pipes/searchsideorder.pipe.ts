import { Pipe, PipeTransform } from '@angular/core';
import { SideOrder } from '../models/side_order';

@Pipe({
  name: 'searchsideorder'
})
export class SearchsideorderPipe implements PipeTransform {

  transform(arreglo: SideOrder[], texto: any): unknown {

    if(texto){
      return arreglo.filter(
        item => JSON.stringify(item).toLowerCase().includes(texto.toLowerCase())
      );
    } else {
      return arreglo;
    }
  }

}
