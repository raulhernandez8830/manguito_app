import { Product } from 'src/app/models/product';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchproducts'
})
export class SearchproductsPipe implements PipeTransform {

  transform(arreglo: Product[], texto: any): any {

    if(texto){
      return arreglo.filter(
        item => JSON.stringify(item).toLowerCase().includes(texto.toLowerCase())
      );
    } else {
      return arreglo;
    }


  }

}
