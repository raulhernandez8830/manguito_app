import { ToastController, LoadingController } from '@ionic/angular';
import { UserStore } from './../../models/user_store';
import { PedidosService } from './../../services/pedidos.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import SwiperCore from 'swiper/core';
import { Pedido } from 'src/app/models/pedido';
import { Chart } from 'chart.js';
import { PushService } from 'src/app/services/push.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})

export class HomePage implements OnInit {


  pedidos: any[] = [];
  @ViewChild('barChart') barChart;
  @ViewChild('doughnut') doughnut;


  bars: any;
  colorArray: any;
  donut: any;


  sliderConfig = {
    spaceBetween: 30,
    centeredSlides: true,
    slidesPerView: 1.8,
    initialSlide: 1,

  }

  constructor(private pedidoservice: PedidosService,   private pushservice: PushService,
     private toastctr: ToastController, private loadctr: LoadingController) {

  }

  ngOnInit() {
    this.getPedidos();
  }

  ionViewDidEnter() {
    this.createBarChart();
    this.createDoughnut();
  }

  createBarChart() {
    this.bars = new Chart(this.barChart.nativeElement, {
      type: 'bar',
      data: {
        labels: ['S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8'],
        datasets: [{
          label: 'Productos mas vendidos',
          data: [2.5, 3.8, 5, 6.9, 6.9, 7.5, 10, 17],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
        ],          borderColor: 'rgb(38, 194, 129)',// array should have same number of elements as number of dataset
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }

  generateColorArray(num) {
    this.colorArray = [];
    for (let i = 0; i < num; i++) {
      this.colorArray.push('#' + Math.floor(Math.random() * 16777215).toString(16));
    }
  }

  createDoughnut() {
    this.donut = new Chart(this.doughnut.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ['S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8'],
        datasets: [{
          label: 'Categorias mas solicitadas',
          data: [2.5, 3.8, 5, 6.9, 6.9, 7.5, 10, 17],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
        ],
          borderColor: this.colorArray,// array should have same number of elements as number of dataset
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }

  areaChart() {

  }


  onSwiper(swiper) {
    console.log(swiper);
  }
  onSlideChange() {
    console.log('slide change');
  }

  getPedidos() {
    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));
    this.pedidoservice.pedidos$.subscribe(
      response => {
        this.pedidos = response;
      }
    )
  }


  async successToast() {
    const toast = await this.toastctr.create({
      message: 'Estado de pedido actualizado',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async errorToast() {
    const toast = await this.toastctr.create({
      message: 'Error al guardar datos.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  // LOADER
  async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Actualizando estado...',
      duration: 2000,
      spinner: 'bubbles'

    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }


  imprimir() {

  }

  detalles() {

  }

  aceptarPedido(pedido) {

    // obtenemos el usuario para poder guardar la informacion de user/store
    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));

    this.presentLoading();
    pedido.estado_id = 3;
    this.pedidoservice.actualizarEstadoPedido(pedido).subscribe(
      response => {

      },
      err => {

      },
      () => {
        this.successToast();
        this.getPedidos();

        // actualizamos el conteo de los pedidos recibidos que no han sido aceptados
        this.pedidoservice.getPedidosNuevos(userstore.store_id).subscribe(
          response => {
            this.pedidoservice.setConteo(response.length);
            console.log(response.length);

          }
        )

        // llamada al metodo de las push para enviar push al usuario
        let title = "Pedido aceptado";
        let body = "Su pedido fue aceptado por el restaurante y se notificara cuando este listo para ser entregado";
        this.pushservice.pusNotificationhUpdateEstado(pedido.userToken, title, body).subscribe(
          response => {

          },
          err => {

          },
          () => {

          }
        )

      }
    )
  }

  rechazarPedido(pedido) {
    this.presentLoading();
    pedido.estado_id = 7;
    this.pedidoservice.actualizarEstadoPedido(pedido).subscribe(
      response => {

      },
      err => {

      },
      () => {
        this.successToast();
        this.getPedidos();

         // llamada al metodo de las push para enviar push al usuario
         let title = "Pedido rechazado";
         let body = "Su pedido ha sido rechazado por el restaurante";
         this.pushservice.pusNotificationhUpdateEstado(pedido.userToken, title, body).subscribe(
           response => {

           },
           err => {

           },
           () => {

           }
         )
      }
    )
  }

  pedidoListo(pedido) {
    this.presentLoading();
    pedido.estado_id = 4;
    this.pedidoservice.actualizarEstadoPedido(pedido).subscribe(
      response => {

      },
      err => {

      },
      () => {
        this.successToast();
        this.getPedidos();

         // llamada al metodo de las push para enviar push al usuario
         let title = "Pedido Listo!!";
         let body = "Su pedido se encuentra listo para ser entregado";
         this.pushservice.pusNotificationhUpdateEstado(pedido.userToken, title, body).subscribe(
           response => {

           },
           err => {

           },
           () => {

           }
         )
      }
    )
  }



  checkScreen() {

    if(window.innerWidth>=960){
          return 2.125;
      }else{
          return 1.125;
      }
  }


}
