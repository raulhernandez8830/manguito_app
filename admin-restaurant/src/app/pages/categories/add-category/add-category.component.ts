import { CategoriesService } from './../../../services/categories.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoadingController, ToastController } from '@ionic/angular';
import { UserStore } from 'src/app/models/user_store';
import { Category } from 'src/app/models/category';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss'],
})
export class AddCategoryComponent implements OnInit {

  frm_addcategoria: FormGroup;
  imagen: File;

  constructor(private invcategoriaservice: CategoriesService,
    private toastctr: ToastController,
    private loadctr: LoadingController) {
    this.frm_addcategoria = new FormGroup({
      'nombre': new FormControl('', Validators.required),
      'descripcion': new FormControl(),
      'imagen': new FormControl()

    })
  }

  ngOnInit() {}

  async successToast() {
    const toast = await this.toastctr.create({
      message: 'Categoria agregada con exito.',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async errorToast() {
    const toast = await this.toastctr.create({
      message: 'Error al guardar datos.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  // LOADER
  async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Guardando información...',
      duration: 2000,
      spinner: 'bubbles'
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }

  onSubmit() {

    // obtenemos el usuario para poder guardar la informacion de user/store
    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));
    console.log(userstore);

    // objeto categoria para llamar al service de guardado en la base de datos
    let categoria: Category = new Category;
    categoria = this.frm_addcategoria.value;
    categoria.store_id = userstore.store_id;
    categoria.user_id = userstore.id;

    this.frm_addcategoria.controls['imagen'].setValue(this.imagen);



    this.presentLoading();
    this.invcategoriaservice.saveCategoria(categoria,this.imagen).subscribe(

      response => {console.log(response);},
      err => {
        this.errorToast();
      },
      () => {
        this.successToast();
        this.frm_addcategoria.reset();
      }
    )
  }

  onChangeInputFile(e){
    this.imagen = e.target.files[0];

  }

}
