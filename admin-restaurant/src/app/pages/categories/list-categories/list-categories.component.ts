import { DetailsCategoryComponent } from './../details-category/details-category.component';
import { Category } from 'src/app/models/category';
import { Component, OnInit } from '@angular/core';
import { CategoriesService } from 'src/app/services/categories.service';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-list-categories',
  templateUrl: './list-categories.component.html',
  styleUrls: ['./list-categories.component.scss'],
})
export class ListCategoriesComponent implements OnInit {


  invcategorias: Category[] = [];
  texto: any;
  skeleton = true;
  alert_vacio = false;
  url = environment.urlcategorias;

  constructor(private categoriaservice: CategoriesService, private modalController: ModalController,
    private loadctr: LoadingController,private toastctr: ToastController,
              private modalctr: ModalController) { }

  ngOnInit() {
    // en la primer carga obtenemos las categorias de la base de datos
    this.getCategorias();
  }

  // obtener las categorias listadas en la base de datos
  getCategorias() {
    let userstore = JSON.parse(localStorage.getItem('usuariostore'));
    this.categoriaservice.getCategorias(userstore).subscribe(
      response => {
        this.invcategorias = response;
      },
      err => {},
      () => {
        if(this.invcategorias.length == 0) {
          this.alert_vacio = true;
          this.skeleton = false;
        } else {
          this.skeleton = false;
        }
      }
    )
  }

  // eliminar una categoria
  deleteCategory(c: Category) {
    this.presentLoading();
    this.categoriaservice.deleteCategory(c).subscribe(
      response => {

      },
      err => {},
      () => {
        this.deleteSuccess();
        this.getCategorias();
      }
    )
  }

  async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Eliminando categoria...',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  async editarCategoria(c) {

      const modal = await this.modalController.create({
        component: DetailsCategoryComponent,
        cssClass: 'editarCatModal',
        componentProps: {
          categoria: c
        }
      });
      modal.onDidDismiss().then(
        (data)=> {
          this.getCategorias();
        }
      )
      return await modal.present();
  }


  async deleteSuccess() {
    const toast = await this.toastctr.create({
      message: 'Categoria eliminada con exito.',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async errorSuccess() {
    const toast = await this.toastctr.create({
      message: 'error al eliminar datos.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

}
