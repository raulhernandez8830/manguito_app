import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.page.html',
  styleUrls: ['./categories.page.scss'],
})
export class CategoriesPage implements OnInit {

  titulo = 'categorias';
  selectedSegment = 'listado';

  constructor(private router: Router) { }

  ngOnInit() {
  }

  segmentChanged(value) {
    this.selectedSegment = value;
  }
}
