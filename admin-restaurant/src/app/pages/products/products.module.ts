import { SideOrderComponent } from './side-order/side-order.component';
import { PipesModule } from './../../pipes/pipes.module';
import { ListProductsComponent } from './list-products/list-products.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { AddProductComponent } from './add-product/add-product.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductsPageRoutingModule } from './products-routing.module';

import { ProductsPage } from './products.page';
import { ComponentsModule } from 'src/app/components/components.module';
import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ProductsPageRoutingModule,
    PipesModule,
    ComponentsModule,
    NgxPaginationModule

  ],
  declarations: [ProductsPage, AddProductComponent, ProductDetailComponent, ListProductsComponent, SideOrderComponent],
  exports: [
    ProductDetailComponent, AddProductComponent, ListProductsComponent
  ]
})
export class ProductsPageModule {}
