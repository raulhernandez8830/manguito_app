import { SideorderService } from './../../../services/sideorder.service';
import { SideOrder } from './../../../models/side_order';
import { TipoMenu } from './../../../models/tipo_menu';
import { ProductsService } from './../../../services/products.service';
import { Category } from 'src/app/models/category';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavParams, ToastController, LoadingController, ModalController } from '@ionic/angular';
import { Product } from 'src/app/models/product';
import { CategoriesService } from 'src/app/services/categories.service';
import { environment } from 'src/environments/environment';
import { SideOrderComponent } from '../side-order/side-order.component';
interface Usere {
  id: number;
  first: string;
  last: string;
}
@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
})
export class ProductDetailComponent implements OnInit {

  categoria: number;
  frm_edit_product: FormGroup;
  prod: Product;
  categories: Category[] = [];
  imagenSeleccionada: File;
  tiposmenu: TipoMenu[] = [];
  urlproducts = environment.urlproducts;
  sideorders: SideOrder[] = [];
  @ViewChild('dia') span:ElementRef;
  sideorderprod: SideOrder[] = [];

  constructor(private router: Router, private modalctr: ModalController,
    private productservice: ProductsService,
    private loadctr: LoadingController,
    private toastctr: ToastController,
    private sideorderservice: SideorderService,
    private categoriaservice: CategoriesService,
    private navparam: NavParams) {
      this.frm_edit_product = new FormGroup({
      'image':            new FormControl(''),
      'nombre':           new FormControl('N', Validators.required),
      'descripcion':      new FormControl('NA ' ),
      'precio':           new FormControl(' ', Validators.required),
      'precio_oferta':    new FormControl('0'),
      'categoria_id':     new FormControl('', Validators.required),
      'tipo_menu':        new FormControl('', Validators.required),
      'id':               new FormControl('', Validators.required),
      'estado':           new FormControl('N', Validators.required),
      'dia':              new FormControl(),
      'always_available': new FormControl()

      });



   }



    ngOnInit() {

      this.categories = this.navparam.get('categories');
      this.prod = this.navparam.get('product');
      this.frm_edit_product.patchValue(this.prod);

      this.tiposmenu = this.navparam.get('tiposmenu');


      this.getSideOrderByProduct(this.prod);


  }



  cancelaredicion() {
    this.modalctr.dismiss();
  }



  // guardamos edicion del producto
  guardarEdicion() {

    this.frm_edit_product.controls['categoria_id'].setValue(
      (<HTMLInputElement>document.getElementById("categoriaprod")).value
    );

    this.frm_edit_product.controls['estado'].setValue(
      (<HTMLInputElement>document.getElementById("estado")).value
    );


    if(this.frm_edit_product.controls['descripcion'].value === null){
      this.frm_edit_product.controls['descripcion'].setValue(' ');
    }

    if(this.frm_edit_product.controls['always_available'].value === null){
      this.frm_edit_product.controls['always_available'].setValue(false);
    }



    this.frm_edit_product.controls['dia'].setValue(
      (<HTMLInputElement>document.getElementById("dia")).value
    );

    this.frm_edit_product.controls['id'].setValue(this.prod.id);

    this.frm_edit_product.controls['tipo_menu'].setValue(
      (<HTMLInputElement>document.getElementById("tipomenu")).value
    );
    this.presentLoading();
    let userstore = JSON.parse(localStorage.getItem('usuariostore'));

    if(typeof this.imagenSeleccionada == 'undefined') {
      this.frm_edit_product.controls['image'].setValue('');
    } else {
      this.frm_edit_product.controls['image'].setValue(this.imagenSeleccionada);
    }


    console.log(this.frm_edit_product.value);

    //llamada al service para guardar la edicion
    this.productservice.editProduct(userstore,this.frm_edit_product.value, this.sideorders).subscribe(
      response => {

      },
      err => {
        this.errorToast();
      },
      () => {
        this.productservice.limpiarSideOrder();
        this.successToast();
        this.modalctr.dismiss();
        this.router.navigateByUrl('products');
      }
    )
  }



  async successToast() {
    const toast = await this.toastctr.create({
      message: 'Producto editado con exito.',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async errorToast() {
    const toast = await this.toastctr.create({
      message: 'Error al guardar datos.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  // LOADER
  async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Guardando información...',
      duration: 2000,
      spinner: 'dots'
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }

  onChangeInputFile(e){
    this.imagenSeleccionada = e.target.files[0];
  }

  deleteSide(side) {
    this.productservice.deleteSide(side);
  }


  //modal para agregar side orders al menu
  async addSideOrders() {
    const modal = await this.modalctr.create({
      component: SideOrderComponent,
      cssClass: 'fullscreen',

    });
    modal.onDidDismiss().then(
      (data) => {
        this.productservice.side$.subscribe(
          response => {
            this.sideorders = response;
          }
        )
      }
    )
    return await modal.present();
  }

  cambioAvailableMenu(evt){
    if(evt.target.checked) {

      this.prod.dia = '';
    }
  }





  eliminarSideProd(side,i) {

    this.presentLoading();

    // llamada al metodo de la api para eliminar un side del menu
    this.sideorderservice.deleteSideOrderToMenu(side,this.prod).subscribe(
      response => {
        this.sideorderprod.splice(i,1);
      }, err => {},
      () => {
        this.successEliminacionSide();
      }
    )
  }


  getSideOrderByProduct(prod) {
    this.sideorderservice.getSideOrderByProduct(prod).subscribe(
      response => {
        this.sideorderprod = response;
      },
      err => {

      }, () => {

      }
    )
  }

  async successEliminacionSide() {
    const toast = await this.toastctr.create({
      message: 'Side eliminado del menu.',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }


}


