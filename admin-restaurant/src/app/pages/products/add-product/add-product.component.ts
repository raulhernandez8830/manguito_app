import { SideOrder } from './../../../models/side_order';
import { SideOrderComponent } from './../side-order/side-order.component';
import { TipoMenu } from './../../../models/tipo_menu';
import { ProductsService } from './../../../services/products.service';
import { Category } from './../../../models/category';
import { CategoriesService } from './../../../services/categories.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserStore } from 'src/app/models/user_store';
import { LoadingController, ToastController, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss'],
})
export class AddProductComponent implements OnInit {

  frm_add_product: FormGroup;
  barcode_ = true;
  tipomenu = false;
  qrcode_ = true;
  imagenSeleccionada: File;
  categories: Category[] = [];
  tiposmenu: TipoMenu[] = [];
  sideorders: SideOrder[] = [];

  constructor(private categoriaservice: CategoriesService, private modalctr: ModalController,
    private productservice: ProductsService,
    private loadctr: LoadingController,
    private toastctr: ToastController) {
    this.frm_add_product = new FormGroup({

      'image':            new FormControl(''),
      'nombre':           new FormControl('', Validators.required),
      'descripcion':      new FormControl('', ),
      'precio':           new FormControl('', Validators.required),

      'categoria_id':     new FormControl('', Validators.required),
      'tipo_menu':        new FormControl('', Validators.required),
      'dia':              new FormControl(''),
      'always_available': new FormControl('')


    })

    this.frm_add_product.controls['always_available'].patchValue(false);
  }

  ngOnInit() {
    //obtener categorias para listar en el formulario
    this.getCategories();

    //obtener los tipos de menu
    this.getTiposMenu();
  }

  onSubmit() {

    this.presentLoading();

    this.productservice.side$.pipe().subscribe(
      response => {
        this.sideorders = response;
        console.log(this.sideorders);
      }
    )

    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));

    this.frm_add_product.controls['image'].setValue(this.imagenSeleccionada);
    console.log(this.frm_add_product.value);
    this.frm_add_product.controls['descripcion'].setValue(" ");

    //save product
    this.productservice.saveProduct(this.frm_add_product.value, userstore, this.sideorders).subscribe(
      response => {
        console.log(response);
      },
      err=>{
        this.errorToast();
      },
      ()=>{
        this.successToast();
        this.frm_add_product.reset();
        this.sideorders.length = 0;
      }
    )
  }

  onChangeInputFile(e) {
    this.imagenSeleccionada = e.target.files[0];
  }

  getCategories() {
    let userstore = JSON.parse(localStorage.getItem('usuariostore'));
    this.categoriaservice.getCategorias(userstore).subscribe(
      response => {
        this.categories = response;
      },
      err => {},
      () => {}
    )
  }

  async successToast() {
    const toast = await this.toastctr.create({
      message: 'Producto agregado con exito.',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async errorToast() {
    const toast = await this.toastctr.create({
      message: 'Error al guardar datos.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  // LOADER
  async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Guardando información...',
      duration: 2000,
      spinner: 'dots'
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }

  onChangeCheckBox(e) {
    if (e.target.checked) {
      this.tipomenu = true;

    } else  {
      this.tipomenu = false;

    }
  }

  getTiposMenu() {
    let userstore = JSON.parse(localStorage.getItem('usuariostore'));

    this.productservice.getTiposMenu(userstore).subscribe(
      response => {
        this.tiposmenu = response;
      },
      err => {},
      () => {}

    )
  }

  //modal para agregar side orders al menu
  async addSideOrders() {
    const modal = await this.modalctr.create({
      component: SideOrderComponent,
      cssClass: 'editarCatModal',

    });
    modal.onDidDismiss().then(
      (data) => {
        this.productservice.side$.subscribe(
          response => {
            this.sideorders = response;
          }
        )
      }
    )
    return await modal.present();
  }

  deleteSide(index) {
    this.sideorders.splice(index,1);
  }

  cambioAvailableMenu(evt) {
    if(evt.target.checked) {
      this.frm_add_product.controls['dia'].disable();
    } else {
      this.frm_add_product.controls['dia'].enable();
      this.frm_add_product.controls['always_available'].setValue(false);
    }
  }

}
