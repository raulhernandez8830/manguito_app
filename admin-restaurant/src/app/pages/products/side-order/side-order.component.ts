import { ProductsService } from './../../../services/products.service';
import { TipoSideOrder } from './../../../models/tipo_side_order';
import { SideorderService } from './../../../services/sideorder.service';
import { SideOrderDetail } from './../../../models/side_order_detail';
import { ModalController, LoadingController } from '@ionic/angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { SideOrder } from 'src/app/models/side_order';
import { UserStore } from 'src/app/models/user_store';

@Component({
  selector: 'app-side-order',
  templateUrl: './side-order.component.html',
  styleUrls: ['./side-order.component.scss'],
})
export class SideOrderComponent implements OnInit {

  tipo_side_order: number;
  tipossideorder: TipoSideOrder[] = [];
  sideorders: SideOrder[] = [];
  sideSeleccionados: SideOrder[] = [];
  side_: any;

  isChecked = false;
  masterCheck = false;

  funcionOnChange = false;

  constructor(private sideorderservice: SideorderService, private loadctr: LoadingController,
    private modalctr: ModalController,
    private productservice: ProductsService) {

  }

  ngOnInit() {
    this.getTiposSideOrders();
  }

  // obtener tipos de side order
  getTiposSideOrders() {
    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));
    this.sideorderservice.getTiposSideOrder(userstore).subscribe(
      response => {
        this.tipossideorder = response;
      }
    )
  }

  // obtener los side orders por tipo
  getSideOrdersByTipo(tipo) {
    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));
    this.sideorderservice.getSideOrdersByTipo(tipo, userstore).subscribe(
      response => {
        this.sideorders = response;
      }
    )
  }

   // LOADER
   async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Cargando información...',
      duration: 2000,
      spinner: 'dots'
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }


  tipochange(e) {
    this.getSideOrdersByTipo(e.target.value);
    this.presentLoading();
  }

  finalizarSeleccion() {

    console.log(this.sideSeleccionados);
    this.modalctr.dismiss(this.sideSeleccionados);
  }

  // funcion on change
  addSide(side: SideOrder, i, e) {
    if(e.target.checked) {

      if(this.funcionOnChange === false) {
        this.sideSeleccionados.push(side);
        this.productservice.addSideOrderBehavior(side);
      } else {

      }


      console.log(this.sideSeleccionados);
    } else {
      if(this.sideSeleccionados.length === 1){
        this.sideSeleccionados.length = 0
      } else {
        this.productservice.deleteSide(side)
        this.sideSeleccionados.splice(i, 1);
        console.log(this.sideSeleccionados);
      }


    }
  }

  cancelaredicion() {
    this.modalctr.dismiss();
  }

  // funcion select all
  cambio(e) {
    this.isChecked = true;


    if(e.target.checked) {

      this.funcionOnChange = true;

      this.sideorders.forEach(element => {
        this.productservice.deleteSide(element);

      });

      this.sideorders.forEach(element => {
        this.sideSeleccionados.push(element);
        this.productservice.addSideOrderBehavior(element);

      });

    } else {
      this.funcionOnChange = false;
      this.isChecked = false;
      this.sideorders.forEach(element => {
        this.productservice.deleteSide(element);
      });
    }
  }



  // getSideOrdersSeleccionados() {
  //   this.productservice.side$.subscribe(
  //     response => this.
  //   )
  // }






}
