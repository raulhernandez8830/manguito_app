import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {

  titulo = 'menus';
  estado = 'edicion';
  selectedSegment = 'listado';


  constructor() { }

  ngOnInit() {
  }

  segmentChanged(value) {
    this.selectedSegment = value;
  }
}
