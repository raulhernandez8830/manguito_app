import { SideOrder } from 'src/app/models/side_order';
import { SideorderService } from 'src/app/services/sideorder.service';
import { ModalPedidoDetailComponent } from './../../pedidos/modal-pedido-detail/modal-pedido-detail.component';
import { TipoMenu } from './../../../models/tipo_menu';

import { Category } from './../../../models/category';
import { CategoriesService } from './../../../services/categories.service';
import { ProductDetailComponent } from './../product-detail/product-detail.component';
import { ModalController, NavParams, LoadingController, ToastController } from '@ionic/angular';
import { Product } from './../../../models/product';
import { ProductsService } from './../../../services/products.service';
import { Component, Input, OnInit, Output } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.scss'],
})
export class ListProductsComponent implements OnInit {

  @Input() estado: string;
  @Input() manual: boolean;
  products: Product[] = [];
  categories: Category[] = [];
  urlproducts = environment.urlproducts;
  texto: string;
  tiposmenu: TipoMenu[] = [];
  frmnuevo = false;
  p = 1;
  categoria: number;
  sideorderprod: SideOrder[] = [];


  constructor(private productservice: ProductsService, private sideorderservice:SideorderService,
    private toastctr: ToastController,
    private loadctr: LoadingController,
    private categorieservice: CategoriesService,
    private modalctr: ModalController) { }

  ngOnInit() {

    this.getTiposMenu();
    this.getProducts();
    let userstore = JSON.parse(localStorage.getItem('usuariostore'));
    this.categorieservice.getCategorias(userstore).subscribe(
      response => {
        this.categories = response;
      },
      err => {},
      () =>{}
    )
  }

  getProducts() {

    let userstore = JSON.parse(localStorage.getItem('usuariostore'));
    this.productservice.getProducts(userstore).subscribe(
      response => {
        this.products = response;
      },
      err => {},
      () => {}
    )
  }



  async editarProduct(product: Product) {



    console.log(product);
    console.log(this.sideorderprod);
    const modal = await this.modalctr.create({
      component: ProductDetailComponent,
      componentProps: {
        'product': product,
        'categories': this.categories,
        'tiposmenu': this.tiposmenu,
        'sideprod': this.sideorderprod
      },
      cssClass: 'modaledicionproducto'
    });

    modal.onDidDismiss().then(
      () => {
        this.getProducts();
      }
    )
    return await modal.present();
  }

  getTiposMenu() {
    let userstore = JSON.parse(localStorage.getItem('usuariostore'));

    this.productservice.getTiposMenu(userstore).subscribe(
      response => {
        this.tiposmenu = response;
      },
      err => {},
      ()=> {}
    )
  }


  // seleccionar producto para deal
  seleccionarProductoByDeal(product: Product){
    this.modalctr.dismiss(product);
  }

  async successToast() {
    const toast = await this.toastctr.create({
      message: 'Producto eliminado con exito.',
      duration: 3000,
      color: 'success'
    });
    toast.present();
  }

  async errorToast() {
    const toast = await this.toastctr.create({
      message: 'Error al eliminar datos.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  // LOADER
  async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Eliminando información...',
      duration: 3000,
      spinner: 'dots'
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }

  deleteProduct(product) {
    this.presentLoading();
    this.productservice.deleteProduct(product).subscribe(
      response  => {},
      err => {
        this.errorToast();
      },
      () => {
        this.successToast();
        this.getProducts();
      }
    )
  }


  convertToLowerCase(evt) {
    this.texto = evt.toLowerCase();
  }

  categoriaSeleccionada(evt) {
    console.log(evt.target.value);
    let categoria = parseInt(evt.target.value);
    let userstore = JSON.parse(localStorage.getItem('usuariostore'));
    if(categoria === 0) {
      this.productservice.getProducts(userstore)
      .subscribe(
        response => {
          console.log(response);
          this.products = response;
        }
      )
    } else {
      this.productservice.getProducts(userstore).pipe(
        map(data => data.filter(p => p.categoria_id === categoria))
      ).subscribe(
        response => {
          console.log(response);
          this.products = response;
        }
      )
    }


    // let obs_product = of(this.products);
    // obs_product.pipe(map(data => data.filter(p => p.categoria_id === evt.target.value)
    // )).subscribe(
    //   response => {
    //     this.products = response;
    //     console.log(this.products);
    //   }
    // )

  }

  async addProductToPedido(product) {
    const modal = await this.modalctr.create({
      component: ModalPedidoDetailComponent,
      componentProps: {
        'product': product
      },
      cssClass: 'fullscreen'
    });

    return await modal.present();
  }



}


