import { DetailTipoMenuComponent } from './../detail-tipo-menu/detail-tipo-menu.component';
import { ModalController, LoadingController, ToastController } from '@ionic/angular';
import { TipoMenu } from './../../../models/tipo_menu';
import { ProductsService } from './../../../services/products.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-tipos-menu',
  templateUrl: './list-tipos-menu.component.html',
  styleUrls: ['./list-tipos-menu.component.scss'],
})
export class ListTiposMenuComponent implements OnInit {

  tiposmenu: TipoMenu[] = [];
  texto: string;

  constructor(private productservice: ProductsService, private loadctr: LoadingController,
     private toastctr: ToastController,
     private modalctr: ModalController) { }

  ngOnInit() {
    this.getTiposMenu();
  }

  // get tipos de menu
  getTiposMenu() {
    let userstore = JSON.parse(localStorage.getItem('usuariostore'));
    this.productservice.getTiposMenu(userstore).subscribe(
      response => {
        this.tiposmenu = response;
      },
      err => {},
      () => {}
    );
  }



  async editarTipoMenu(tipomenu) {
    const modal = await this.modalctr.create({
      component: DetailTipoMenuComponent,
      componentProps: {
        'tipomenu': tipomenu
      },
      cssClass: 'modaledicionproducto'
    });
    modal.onDidDismiss().then(
      () => {
        this.getTiposMenu();
      }
    );
    return await modal.present();
  }

  async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Eliminando tipo de menu...',
      duration: 3000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  delete(tipo) {
    this.presentLoading();
    this.productservice.deleteTipoMenu(tipo).subscribe(
      response => {

      },
      err => {
        this.errorSuccess();
      },
      () => {
        this.getTiposMenu();
        this.deleteSuccess();
      }
    );
  }

  async deleteSuccess() {
    const toast = await this.toastctr.create({
      message: 'Tipo de menu eliminado con exito.',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async errorSuccess() {
    const toast = await this.toastctr.create({
      message: 'Error al eliminar datos.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

}
