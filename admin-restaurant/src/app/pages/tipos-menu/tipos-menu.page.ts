import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tipos-menu',
  templateUrl: './tipos-menu.page.html',
  styleUrls: ['./tipos-menu.page.scss'],
})
export class TiposMenuPage implements OnInit {

  titulo = 'Tipo Menu';
  selectedSegment = 'listado';


  constructor() { }

  ngOnInit() {
  }

  segmentChanged(value) {
    this.selectedSegment = value;
  }

}
