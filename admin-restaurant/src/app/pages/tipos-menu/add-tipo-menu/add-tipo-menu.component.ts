import { ToastController, LoadingController } from '@ionic/angular';
import { TipoMenu } from './../../../models/tipo_menu';
import { CategoriesService } from './../../../services/categories.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { UserStore } from 'src/app/models/user_store';

@Component({
  selector: 'app-add-tipo-menu',
  templateUrl: './add-tipo-menu.component.html',
  styleUrls: ['./add-tipo-menu.component.scss'],
})
export class AddTipoMenuComponent implements OnInit {

  frm_addtipomenu: FormGroup;

  constructor(private categoriaservice: CategoriesService, private loadctr: LoadingController,
    private toastctr: ToastController) {
    this.frm_addtipomenu = new FormGroup({
      'nombre': new FormControl('', Validators.required),
      'descripcion': new FormControl('')
    });
  }

  ngOnInit() {}

    // LOADER
    async presentLoading() {
      const loading = await this.loadctr.create({
        cssClass: 'my-custom-class',
        message: 'Guardando información...',
        duration: 2000,
        spinner: 'circular'
      });
      await loading.present();

      const { role, data } = await loading.onDidDismiss();

    }

  // guardar un tipo de menu para el restaurante
  saveTipoMenu() {
    this.presentLoading();

    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));

    let tipomenu: TipoMenu = new TipoMenu;
    tipomenu = this.frm_addtipomenu.value;
    tipomenu.store_id = userstore.store_id;
    tipomenu.user_id = userstore.id;

    // llamada al service para guardar el tipo de menu
    this.categoriaservice.saveTipoMenu(tipomenu).subscribe(
      response => {},
      err => {
        this.errorToast();
      },
      () => {
        this.successToast();
        this.frm_addtipomenu.reset();
      }
    )
  }


  async successToast() {
    const toast = await this.toastctr.create({
      message: 'Tipo de menu agregado con exito.',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async errorToast() {
    const toast = await this.toastctr.create({
      message: 'Error al guardar datos.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }


}
