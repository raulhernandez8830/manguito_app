import { PipesModule } from './../../pipes/pipes.module';
import { DetailTipoMenuComponent } from './detail-tipo-menu/detail-tipo-menu.component';
import { ListTiposMenuComponent } from './list-tipos-menu/list-tipos-menu.component';
import { AddTipoMenuComponent } from './add-tipo-menu/add-tipo-menu.component';
import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TiposMenuPageRoutingModule } from './tipos-menu-routing.module';

import { TiposMenuPage } from './tipos-menu.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    TiposMenuPageRoutingModule,
    ComponentsModule,
    PipesModule
  ],
  declarations: [TiposMenuPage, AddTipoMenuComponent, ListTiposMenuComponent, DetailTipoMenuComponent]
})
export class TiposMenuPageModule {}
