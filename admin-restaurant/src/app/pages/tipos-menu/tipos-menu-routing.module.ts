import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TiposMenuPage } from './tipos-menu.page';

const routes: Routes = [
  {
    path: '',
    component: TiposMenuPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TiposMenuPageRoutingModule {}
