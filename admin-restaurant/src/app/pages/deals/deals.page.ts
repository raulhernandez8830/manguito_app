import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-deals',
  templateUrl: './deals.page.html',
  styleUrls: ['./deals.page.scss'],
})
export class DealsPage implements OnInit {

  titulo = 'menus';

  selectedSegment = 'listado';


  constructor() { }

  ngOnInit() {
  }

  segmentChanged(value) {
    this.selectedSegment = value;
  }

}
