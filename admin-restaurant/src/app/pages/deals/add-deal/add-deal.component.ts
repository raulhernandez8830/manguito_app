import { UserDeal } from './../../../models/user_deal';
import { Devices } from './../../../models/devices';
import { UserService } from './../../../services/user.service';
import { UserStore } from './../../../models/user_store';
import { User } from './../../../models/user';
import { DealService } from './../../../services/deal.service';
import { Deal } from './../../../models/deal';
import { ModalProductsComponent } from './../modal-products/modal-products.component';
import { ModalController, LoadingController, ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Product } from 'src/app/models/product';
import { environment } from 'src/environments/environment';
import { PushService } from 'src/app/services/push.service';

@Component({
  selector: 'app-add-deal',
  templateUrl: './add-deal.component.html',
  styleUrls: ['./add-deal.component.scss'],
})


export class AddDealComponent implements OnInit {

  frm_deal: FormGroup;
  pedidoseleccionado = false;
  product: Product  = new Product();
  urlproducts = environment.urlproducts;
  users: UserDeal[] = [];
  device_any: any[] = [];

  constructor(private modalctr: ModalController, private userservice: UserService,
    private dealservice: DealService, private pushservice: PushService,
    private loadctr: LoadingController,
    private toastctr: ToastController) {
    this.frm_deal = new FormGroup({
      'descripcion': new FormControl('', Validators.required),
      'descuento': new FormControl('', Validators.required),
      'inicio': new FormControl('', Validators.required),
      'fin': new FormControl('', Validators.required)
    });
  }

  ngOnInit() {}

  async successToast() {
    const toast = await this.toastctr.create({
      message: 'Deal agregada con exito.',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async errorToast() {
    const toast = await this.toastctr.create({
      message: 'Error al guardar datos.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  // LOADER
  async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Guardando información...',
      duration: 2000,
      spinner: 'bubbles'
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }

  saveDeal() {
    let dealdto: Deal = new Deal();
    dealdto = this.frm_deal.value;
    dealdto.product_id = this.product.id
    this.presentLoading();

    this.dealservice.saveDeal(dealdto).subscribe(
      response => {

      },
      err => {
        this.errorToast();
      },
      () => {
        this.successToast();
        this.frm_deal.reset();
        this.pedidoseleccionado = false;


        // enviar push del deal creado
       // this.pushservice.pusNotificationhUpdateEstado()

        let userstore: UserStore = new UserStore();
        userstore = JSON.parse(localStorage.getItem('usuariostore'));


        // busco todos los devices registrados en la base de datos
        let devices: Devices[] = [];
        this.userservice.getDevices(userstore.store_id).subscribe(
          response => {
            devices = response;

          },
          err => {

          },
          () => {
              // creo un arreglo de devices con el nombre de objeto player_id que es lo que recibe el push
            devices.forEach(element => {
              let u: UserDeal = new UserDeal();
              u.player_id = element.deviceToken;
              console.log(u.player_id);
              this.users.push(u);
              this.device_any.push(element.deviceToken);
            });

            console.log(this.users);

            // enviar push notification de las deals
            this.pushservice.pushNotificationDealFuegoPicante(userstore.store, dealdto.descripcion,this.users).subscribe(
              response=> {
                console.log(response);
              }
            )
          }
        )







      }
    )
  }

  async buscarProducto() {
    const modal = await this.modalctr.create({
      component: ModalProductsComponent,
      cssClass: 'fullscreen'
    });

    modal.onDidDismiss().then(
      (data) => {
        this.product = data['data'];
        if(this.product) {
          console.log(this.product);
          this.pedidoseleccionado = true;
        }

      }
    )

    return await modal.present();
  }

}
