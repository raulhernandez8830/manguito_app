import { ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal-products',
  templateUrl: './modal-products.component.html',
  styleUrls: ['./modal-products.component.scss'],
})
export class ModalProductsComponent implements OnInit {

  estado = 'listado';

  constructor(private modalctr: ModalController) { }

  ngOnInit() {}

  modalDismiss() {
    this.modalctr.dismiss();
  }

}
