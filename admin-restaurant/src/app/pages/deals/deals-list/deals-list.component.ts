import { ToastController, LoadingController } from '@ionic/angular';
import { DealService } from './../../../services/deal.service';
import { Component, OnInit } from '@angular/core';
import { UserStore } from 'src/app/models/user_store';
import { Deal } from 'src/app/models/deal';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-deals-list',
  templateUrl: './deals-list.component.html',
  styleUrls: ['./deals-list.component.scss'],
})
export class DealsListComponent implements OnInit {

  deals: any[] = [];
  urlproducts = environment.urlproducts;


  constructor(private dealsservice: DealService, private loadctr: LoadingController,
    private toastctr: ToastController,) { }

  ngOnInit() {
    this.getDeals();
  }

  //get deals
  getDeals() {
    // obtenemos el usuario para poder guardar la informacion de user/store
    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));
    console.log(userstore);
    this.dealsservice.getDeals(userstore.store_id).subscribe(
      response => {
        this.deals = response;
      }
    )
  }

  async successToast() {
    const toast = await this.toastctr.create({
      message: 'Deal eliminado con exito.',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async errorToast() {
    const toast = await this.toastctr.create({
      message: 'Error al eliminar el deal.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  // LOADER
  async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Eliminando...',
      duration: 2000,
      spinner: 'bubbles'
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }

  deleteDeal(deal) {
    this.presentLoading();
    this.dealsservice.deleteDeals(deal).subscribe(
      response => {

      },
      err => {
        this.errorToast()
      },
      () => {
        this.successToast();
        this.getDeals();
      }
    )

  }
}
