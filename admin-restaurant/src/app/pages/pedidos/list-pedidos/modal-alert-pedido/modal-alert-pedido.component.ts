import { ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal-alert-pedido',
  templateUrl: './modal-alert-pedido.component.html',
  styleUrls: ['./modal-alert-pedido.component.scss'],
})
export class ModalAlertPedidoComponent implements OnInit {

  audio = new Audio();
  reproducir = true;

  constructor(private modalctr: ModalController) { }

  ngOnInit() {
    this.loadAudio();
    setInterval(()=>{
      if(this.reproducir) {
        this.audio.play();
      } else {
        this.audio.pause();
      }

    },2000)
  }

  cerrar() {
    this.reproducir = false;
    this.audio.pause();
    this.audio.currentTime = 0;
    this.modalctr.dismiss();
  }

  loadAudio(){

    this.audio.src = "../../../../assets/sonidos/Scoring a Point.wav";
    this.audio.load();

  }

  stopAudio() {
    this.audio.pause();
  }


}
