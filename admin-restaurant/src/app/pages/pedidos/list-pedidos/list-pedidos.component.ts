import { PrintPedidoComponent } from './../print-pedido/print-pedido.component';
import { PushService } from './../../../services/push.service';
import { DetailsPedidoComponent } from './../details-pedido/details-pedido.component';
import { ModalController, ToastController, LoadingController } from '@ionic/angular';
import { PedidosService } from './../../../services/pedidos.service';
import { Component, OnInit } from '@angular/core';
import { UserStore } from 'src/app/models/user_store';
import { ModalAlertPedidoComponent } from './modal-alert-pedido/modal-alert-pedido.component';
import { PrintService } from 'src/app/services/print.service';

@Component({
  selector: 'app-list-pedidos',
  templateUrl: './list-pedidos.component.html',
  styleUrls: ['./list-pedidos.component.scss'],
})
export class ListPedidosComponent implements OnInit {

  pedidos: any[] = [];
  texto: string;
  p: number = 1;
  pedidos_nuevos: any[] = [];
  audio = new Audio();
  reproducir = true;

  bluetoothList:any=[];
  selectedPrinter:any;


  constructor(private pedidoservice: PedidosService, private print:PrintService,
    private pushservice: PushService,
    private toastctr: ToastController,
    private loadctr: LoadingController,
    private modalctr: ModalController ) {



    }

  ngOnInit() {
    this.getPedidos();
    // this.playAudio();
    //this.getOrderRealTime();

    this.listPrinter();

  }

   //This will list all of your bluetooth devices
   listPrinter() {
      this.print.searchBluetoothPrinter()
      .then(resp=>{

        //List of bluetooth device list
        this.bluetoothList=resp;
    });
  }


    //This will store selected bluetooth device mac address
    selectPrinter(macAddress)
    {
      //Selected printer macAddress stored here
      this.selectedPrinter=macAddress;
    }

    //This will print
    printStuff()
    {
      //The text that you want to print
      var myText="Hello hello hello \n\n\n This is a test \n\n\n";
      this.print.sendToBluetoothPrinter(this.selectedPrinter,myText);
    }

  getPedidos() {
    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));
    this.pedidoservice.pedidos$.subscribe(
      response => {
        this.pedidos = response;

        // hacemos la primera carga de los pedidos cuando inicia la aplicacion

      }
    )
  }

  async detalles(p) {
    const modal = await this.modalctr.create({
      component: DetailsPedidoComponent,
      cssClass: 'fullscreen',
      componentProps: {
        pedido: p
      }
    });

    return await modal.present();
  }


  aceptarPedido(pedido) {

    // obtenemos el usuario para poder guardar la informacion de user/store
    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));

    this.presentLoading();
    pedido.estado_id = 3;
    this.pedidoservice.actualizarEstadoPedido(pedido).subscribe(
      response => {

      },
      err => {

      },
      () => {
        this.successToast();
        this.getPedidos();

        // actualizamos el conteo de los pedidos recibidos que no han sido aceptados
        this.pedidoservice.getPedidosNuevos(userstore.store_id).subscribe(
          response => {
            this.pedidoservice.setConteo(response.length);
            console.log(response.length);

          }
        )

        // llamada al metodo de las push para enviar push al usuario
        let title = "Pedido aceptado";
        let body = "Su pedido fue aceptado por el restaurante y se notificara cuando este listo para ser entregado";
        this.pushservice.pusNotificationhUpdateEstado(pedido.userToken, title, body).subscribe(
          response => {

          },
          err => {

          },
          () => {

          }
        )

      }
    )
  }

  rechazarPedido(pedido) {
    this.presentLoading();
    pedido.estado_id = 7;
    this.pedidoservice.actualizarEstadoPedido(pedido).subscribe(
      response => {

      },
      err => {

      },
      () => {
        this.successToast();
        this.getPedidos();

         // llamada al metodo de las push para enviar push al usuario
         let title = "Pedido rechazado";
         let body = "Su pedido ha sido rechazado por el restaurante";
         this.pushservice.pusNotificationhUpdateEstado(pedido.userToken, title, body).subscribe(
           response => {

           },
           err => {

           },
           () => {

           }
         )
      }
    )
  }

  pedidoListo(pedido) {
    this.presentLoading();
    pedido.estado_id = 4;
    this.pedidoservice.actualizarEstadoPedido(pedido).subscribe(
      response => {

      },
      err => {

      },
      () => {
        this.successToast();
        this.getPedidos();

         // llamada al metodo de las push para enviar push al usuario
         let title = "Pedido Listo!!";
         let body = "Su pedido se encuentra listo para ser entregado";
         this.pushservice.pusNotificationhUpdateEstado(pedido.userToken, title, body).subscribe(
           response => {

           },
           err => {

           },
           () => {

           }
         )
      }
    )
  }

  async successToast() {
    const toast = await this.toastctr.create({
      message: 'Estado de pedido actualizado',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async errorToast() {
    const toast = await this.toastctr.create({
      message: 'Error al guardar datos.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  // LOADER
  async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Actualizando estado...',
      duration: 2000,
      spinner: 'bubbles'

    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }

  playAudio(){

    this.audio.src = "../../../../assets/sonidos/Scoring a Point.wav";
    this.audio.load();
    this.audio.play();
  }


  // modal para la notificacion
  async modalNotification() {
    const modal = await this.modalctr.create({
      component: ModalAlertPedidoComponent,

    });

    modal.onDidDismiss().then(() => {


    });

    return await modal.present();
  }


  getOrderRealTime() {
    setInterval(
      () => {

        // obtenemos el usuario para poder guardar la informacion de user/store
        let userstore: UserStore = new UserStore();
        userstore = JSON.parse(localStorage.getItem('usuariostore'));

        // realizamos una consulta para saber si tenemos nuevos pedidos en cola en la base de datos
        this.pedidoservice.getPedidos(userstore.store_id).subscribe(
          response => {
            this.pedidos_nuevos = response;
            console.log(this.pedidos_nuevos);
            //recorrido para comprobar si tenemos pedidos nuevos
            for(var i=0; i< this.pedidos_nuevos.length; i++) {
              if(this.pedidos.some(p => p.id === this.pedidos_nuevos[i].id)){
                console.log('no hay pedidos nuevos');

              } else {
                console.log('pedido nuevo encontrado');
                // llamada a la modal para mostrar que tenemos un nuevo pedido
                this.modalNotification();



                // agregamos el pedido nuevo a la carga inicial
                this.pedidos.unshift(this.pedidos_nuevos[i]);

                // reseteamos los pedidos nuevos
                this.pedidos_nuevos.length = 0 ;

                // actualizamos el conteo de los pedidos recibidos que no han sido aceptados
                this.pedidoservice.getPedidosNuevos(userstore.store_id).subscribe(
                  response => {
                    this.pedidoservice.setConteo(response.length);
                    console.log(response.length);
                    this.pedidoservice.conteo$.subscribe();
                  }
                )
              }
            }
          }
        )

    },12000)
  }


  async imprimir(pedido) {
    const modal = await this.modalctr.create({
      component: PrintPedidoComponent,
      componentProps: {
        'pedido': pedido
      },
      cssClass: 'fullscreen',
    });

    return await modal.present();
  }


}
