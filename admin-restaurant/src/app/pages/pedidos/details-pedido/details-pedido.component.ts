import { SideorderService } from './../../../services/sideorder.service';
import { ModalController, NavParams } from '@ionic/angular';
import { PedidosService } from './../../../services/pedidos.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { SideOrder } from 'src/app/models/side_order';
import { Printer, PrintOptions } from '@ionic-native/printer/ngx';
import * as es6printJS from "print-js";
import { PrintService } from 'src/app/services/print.service';
import * as moment from 'moment';



@Component({
  selector: 'app-details-pedido',
  templateUrl: './details-pedido.component.html',
  styleUrls: ['./details-pedido.component.scss'],
})
export class DetailsPedidoComponent implements OnInit {

  pedido: any;
  detalles: any[] = [];
  urlproducts = environment.urlproducts;
  nombrecliente: string;
  sideorders: any[] = [];

  bluetoothList:any=[];
  selectedPrinter:any;

  constructor(private pedidoservice: PedidosService,  private print:PrintService,
    private printer: Printer,
    private sideservice: SideorderService,
    private modalctr: ModalController, private navparam: NavParams) { }

  ngOnInit() {
    this.pedido = this.navparam.get('pedido');

    this.getDetailPedido();
    this.nombrecliente = this.pedido.nombre_cliente + ' ' + this.pedido.apellido_cliente;
    this.getSideOrdersByPedido();

    this.listPrinter();

    console.log(this.pedido);

  }

   //This will list all of your bluetooth devices
   listPrinter() {
    this.print.searchBluetoothPrinter()
    .then(resp=>{

      //List of bluetooth device list
      this.bluetoothList=resp;

  });

}


  //This will store selected bluetooth device mac address
  selectPrinter(macAddress)
  {
    //Selected printser macAddress stored here
    this.selectedPrinter=macAddress;

  }

  //This will print
  printStuff()
  {
    var  encabezado = `
    ---- Pedido #`+ this.pedido.id+ `----
    \n

    ` ;

    // var detalle = '';
    // this.detalles.forEach(element => {
    //   detalle += '\n' + element.nombre  + 'cantidad: ' + element.cantidad +  '\n' + 'comentario: ' + element.comentario + '\n' + '----------' + '\n'+ '\n' ;
    // });


    // let pie = '\n \n Total: $' + this.pedido.total;


    // let contenido = encabezado + detalle  + pie;

    // console.log(detalle);
    // console.log(contenido);

    var text = '';



    var encabezado = 'cliente: ' + this.pedido.nombre_cliente.trim() + ' ' + this.pedido.apellido_cliente.trim() + '\n' + 'direccion: ' + this.pedido.direccion_envio + '\n' + 'pago: ' + this.pedido.opcion_pago+ '\n \n';

    text+= '\n'+'Pedido #'+  this.pedido.id + '  ' + moment(this.pedido.fecha_creacion).format('MMMM Do YYYY, h:mm:ss a') + '\n \n \n';

    text+= encabezado;

    this.detalles.forEach(element => {
      var com = element.comentario == undefined ? '' : element.comentario.trim();

      text += '\n' + element.cantidad + '   ' + element.nombre.trim()  + '\n' + 'Comentario: ' + com + '\n \n';

    });



    var pie = '\n\nTotal: $' + this.pedido.total.toFixed(2);
    var contenido = text  + pie;



    console.log(contenido);


    //The text that you want to print
    var myText="Hello hello hello \n\n\n This is a test \n\n\n";
    this.print.sendToBluetoothPrinter(this.selectedPrinter,contenido);
  }

  getDetailPedido() {
    this.pedidoservice.getDetailPedido(this.pedido).subscribe(
      response => {
        this.detalles = response;
      }
    );
  }

  dismissModal() {
    this.modalctr.dismiss();
  }

  getSideOrdersByPedido(){
    this.sideservice.getSideOrdersByPedido(this.pedido.id).subscribe(
      response => {
        this.sideorders = response;
      }, err => {

      },
      () => {

      }
    );
  }

  imprimir() {
    const contenidoPrint = document.getElementById('contenidoimpresion');
    this.printer.isAvailable().then((onSuccess) => {
      const options: PrintOptions = {
      };

      const content = 'Hello World';
      this.printer.print(contenidoPrint, options);
  }, (err) => {
      console.log('Error', err);
  });
}

  // print() {
  //   const contenidoPrint = document.getElementById('contenidoimpresion');
  //   this.printer.isAvailable();
  //   let options: PrintOptions = {
  //       name: 'MyDocument',
  //       duplex: true,
  //     };
  //   this.printer.print("hola mundo", options);
  // }

}
