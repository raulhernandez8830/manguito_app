import { UserStore } from './../../../models/user_store';
import { SideOrderDTO } from './../../../models/side_order_dto';
import { SideOrder } from './../../../models/side_order';
import { DetalleDTO } from './../../../models/detalle_dto';
import { PedidosService } from 'src/app/services/pedidos.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalController, ToastController, LoadingController } from '@ionic/angular';
import { ProductsListComponent } from './../products-list/products-list.component';
import { Component, OnInit } from '@angular/core';
import { Pedido } from 'src/app/models/pedido';
import { DetallePedido } from 'src/app/models/detalle_pedido';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-pedido-manual',
  templateUrl: './pedido-manual.component.html',
  styleUrls: ['./pedido-manual.component.scss'],
})
export class PedidoManualComponent implements OnInit {

  frm_add_pedido: FormGroup;
  selectedSegment = 'encabezado';
  detalle_pedido: DetallePedido[] = [];
  urlproducts = environment.urlproducts;
  detalle_pedido_dto: DetalleDTO[] = [];
  sideorders: SideOrder[] = [];
  subtotal = 0;
  total = 0;
  tax = 0;




  constructor(private modalctr: ModalController, private toastctr: ToastController, private loadctr: LoadingController,
    private pedidoservice: PedidosService) {
    this.frm_add_pedido =   new FormGroup({
      'nombre_cliente':     new FormControl('', Validators.required),
      'apellido_cliente':   new FormControl(''),
      'opcion_delivery':    new FormControl(''),
      'opcion_pago':        new FormControl('', Validators.required),
      'direccion_envio':    new FormControl(''),
      'zipcode':            new FormControl(''),
      'city':               new FormControl(''),
      'state':              new FormControl(''),
      'total':              new FormControl(''),
      'total_descuento':    new FormControl(''),
      'tax':                new FormControl('')
    });
  }

  ngOnInit() {}


  async verProductos() {
    const modal = await this.modalctr.create({
      component: ProductsListComponent,
      cssClass: 'fullscreen',
      componentProps: {
        estado: 'true'
      }
    });
    modal.onDidDismiss().then((data)=>{
      this.pedidoservice.detallepedido$.subscribe(
        response => {
          this.detalle_pedido = response;

        }
      );

      this.pedidoservice.detallepedidofull$.subscribe(
        response => {
          console.clear();
          console.log(response);
          this.detalle_pedido_dto = response;
        }
      )

      this.pedidoservice.sideorders$.subscribe(
        response => {
          this.sideorders = response;
        }
      )
    });
    return await modal.present();
  }


  async successToast() {
    const toast = await this.toastctr.create({
      message: 'Pedido manual agregado con exito.',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async errorToast() {
    const toast = await this.toastctr.create({
      message: 'Error al guardar datos.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  // LOADER
  async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Guardando información...',
      duration: 2000,
      spinner: 'bubbles'
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }


  save() {
    this.presentLoading();

    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));
    console.clear();
    let pedido: Pedido = new Pedido();
    pedido = this.frm_add_pedido.value;
    pedido.detalle_pedido = this.detalle_pedido
    pedido.side_orders = this.sideorders;
    pedido.store_id = userstore.store_id;
    pedido.user_id = userstore.id;
    console.log(pedido);

    this.pedidoservice.savePedido(pedido).subscribe(
      response => {

      }, err => {
        this.errorToast();
      }, () => {
        this.frm_add_pedido.reset();
        this.pedidoservice.resetDetallesPedido();
        this.pedidoservice.resetSideOrder();
        this.subtotal = 0;
        this.successToast();
      }
    )
  }

  segmentChanged(value) {
    this.selectedSegment = value;
  }

  getSubTotal() {
    this.detalle_pedido_dto.forEach(element => {
      let operacion = element.product.precio * element.cantidad;
      this.subtotal += operacion;
    });

    this.sideorders.forEach(element => {
      let op = element.precio * 1;
      this.subtotal += op;
    });
  }

  checkOut() {
    this.getSubTotal();
  }

}
