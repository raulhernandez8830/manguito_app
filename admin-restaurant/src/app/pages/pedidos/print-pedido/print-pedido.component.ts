import { Component, OnInit } from '@angular/core';
import { Printer, PrintOptions } from '@ionic-native/printer/ngx';
import { ModalController, NavParams } from '@ionic/angular';
import { PedidosService } from 'src/app/services/pedidos.service';
import { SideorderService } from 'src/app/services/sideorder.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-print-pedido',
  templateUrl: './print-pedido.component.html',
  styleUrls: ['./print-pedido.component.scss'],
})
export class PrintPedidoComponent implements OnInit {

  pedido: any;
  detalles: any[] = [];
  urlproducts = environment.urlproducts;
  nombrecliente: string;
  sideorders: any[] = [];

  constructor(private pedidoservice: PedidosService, private printer: Printer,
    private sideservice: SideorderService,
    private modalctr: ModalController, private navparam: NavParams) { }

  ngOnInit() {
    this.pedido = this.navparam.get('pedido');

    this.getDetailPedido();
    this.nombrecliente = this.pedido.nombre_cliente + ' ' + this.pedido.apellido_cliente;
    this.getSideOrdersByPedido();
    setTimeout(() =>{
      this.imprimir();
    },3000)
  }

  getDetailPedido() {
    this.pedidoservice.getDetailPedido(this.pedido).subscribe(
      response => {
        this.detalles = response;
      }
    );
  }

  dismissModal() {
    this.modalctr.dismiss();
  }

  getSideOrdersByPedido(){
    this.sideservice.getSideOrdersByPedido(this.pedido.id).subscribe(
      response => {
        this.sideorders = response;
      }, err => {

      },
      () => {

      }
    )
  }

  imprimir() {
    const contenidoPrint = document.getElementById('contenidoimpresion');
    this.printer.isAvailable().then((onSuccess) => {
      const options: PrintOptions = {
      };

      const content = 'Hello World';
      this.printer.print(contenidoPrint, options);
  }, (err) => {
      console.log('Error', err);
  });

  // window.print();
}

  print() {
    const contenidoPrint = document.getElementById('contenidoimpresion');
    this.printer.isAvailable();
    let options: PrintOptions = {
        name: 'MyDocument',
        duplex: true,
      };
    this.printer.print("hola mundo", options);
  }

}
