import { SideorderService } from './../../../services/sideorder.service';
import { DetalleDTO } from './../../../models/detalle_dto';
import { PedidosService } from 'src/app/services/pedidos.service';
import { NavParams, ToastController, ModalController, LoadingController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product';
import { environment } from 'src/environments/environment';
import { DetallePedido } from 'src/app/models/detalle_pedido';
import { SideOrder } from 'src/app/models/side_order';

@Component({
  selector: 'app-modal-pedido-detail',
  templateUrl: './modal-pedido-detail.component.html',
  styleUrls: ['./modal-pedido-detail.component.scss'],
})
export class ModalPedidoDetailComponent implements OnInit {

  urlproducts = environment.urlproducts;
  product: Product = new Product()
  product_pedido: any;
  comentario = '   ';
  conteo = 0;
  detalle_pedido: DetallePedido[] = [];
  sideorders: SideOrder[] = [];

  constructor(private navparam: NavParams, private sideorderservice: SideorderService,
    private pedidoservice: PedidosService,
    private toastctr: ToastController,
    private loadctr: LoadingController, private modalctr: ModalController) { }

  ngOnInit() {
    this.product = this.navparam.get('product');
    this.getSideOrdersByProduct();
  }

  getSideOrdersByProduct() {
    this.sideorderservice.getSideOrderByProduct(this.product).subscribe(
      response => {
        this.sideorders = response;
      }
    )
  }

  sumar() {
    this.conteo ++;
  }

  restar() {
    if(this.conteo < 0) {
      this.conteo = 0;
    } else {
      this.conteo --
    }
  }

  async successToast() {
    const toast = await this.toastctr.create({
      message: 'Menu agregado al pedido en proceso.',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async errorToast() {
    const toast = await this.toastctr.create({
      message: 'Error al guardar datos.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  // LOADER
  async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Agregando menu al pedido...',
      duration: 1000,
      spinner: 'crescent'
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }

  addProductPedido() {
    this.presentLoading();
    setTimeout(()=>{
      this.successToast();
      this.modalctr.dismiss();
    },1000)

    let detallepedido: DetallePedido = new DetallePedido();
    let detallepedidodto: DetalleDTO = new DetalleDTO();

    detallepedido.cantidad = this.conteo;
    detallepedido.product_id  = this.product.id;
    detallepedido.comentario = this.comentario;
    this.pedidoservice.setDetalle(detallepedido);

    detallepedidodto.cantidad = this.conteo;
    detallepedidodto.product = this.product;
    detallepedidodto.comentario = this.comentario;
    this.pedidoservice.setDetalleFull(detallepedidodto);


  }

  addSide(side,evt) {
    if(evt.target.checked){
      side.cantidad = 1;
      this.pedidoservice.setSideOrder(side);
    } else {
      this.pedidoservice.deleteSide(side);
    }
  }

}
