import { ModalController } from '@ionic/angular';
import { PedidosService } from './../../../services/pedidos.service';
import { Component, OnInit } from '@angular/core';
import { UserStore } from 'src/app/models/user_store';
import { DetailsPedidoComponent } from '../details-pedido/details-pedido.component';

@Component({
  selector: 'app-pedidos-completados',
  templateUrl: './pedidos-completados.component.html',
  styleUrls: ['./pedidos-completados.component.scss'],
})
export class PedidosCompletadosComponent implements OnInit {

  pedidos: any[] = [];
  texto = '';

  constructor(private pedidoservice: PedidosService, private modalctr: ModalController) { }

  ngOnInit() {
    this.getPedidos();
  }


  getPedidos() {
    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));
    this.pedidoservice.getPedidos(userstore.store_id).subscribe(
      response => {
        this.pedidos = response;
      }
    )
  }

  async detalles(p) {
    const modal = await this.modalctr.create({
      component: DetailsPedidoComponent,
      cssClass: 'fullscreen',
      componentProps: {
        pedido: p
      }
    });
    return await modal.present();
  }

}
