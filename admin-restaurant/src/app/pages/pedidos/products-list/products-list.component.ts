import { ModalController } from '@ionic/angular';
import { UserStore } from './../../../models/user_store';
import { ProductsService } from './../../../services/products.service';
import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product';
import { environment } from 'src/environments/environment';
import { BrowserModule } from '@angular/platform-browser'


@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss'],
})
export class ProductsListComponent implements OnInit {

  products: Product[] = [];
  urlproducts = environment.urlproducts;
  manual = true;

  constructor(private productservice: ProductsService, private modalctr: ModalController) { }

  ngOnInit() {

  }




  modalDismiss() {
    this.modalctr.dismiss();
  }

}
