import { ModalPedidoDetailComponent } from './modal-pedido-detail/modal-pedido-detail.component';
import { ProductsPageModule } from './../products/products.module';
import { PedidoManualComponent } from './pedido-manual/pedido-manual.component';
import { PrintPedidoComponent } from './print-pedido/print-pedido.component';
import { PedidosCompletadosComponent } from './pedidos-completados/pedidos-completados.component';
import { PipesModule } from './../../pipes/pipes.module';
import { ComponentsModule } from './../../components/components.module';
import { DetailsPedidoComponent } from './details-pedido/details-pedido.component';
import { ListPedidosComponent } from './list-pedidos/list-pedidos.component';
import { AddPedidoComponent } from './add-pedido/add-pedido.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { IonicModule } from '@ionic/angular';

import { PedidosPageRoutingModule } from './pedidos-routing.module';

import { PedidosPage } from './pedidos.page';

import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module
import { ProductsListComponent } from './products-list/products-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PedidosPageRoutingModule,
    ComponentsModule,
    PipesModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    ProductsPageModule

  ],
  declarations: [PedidosPage, AddPedidoComponent, ListPedidosComponent, PedidoManualComponent, ModalPedidoDetailComponent,
    DetailsPedidoComponent, PedidosCompletadosComponent, PrintPedidoComponent, ProductsListComponent]
})
export class PedidosPageModule {}
