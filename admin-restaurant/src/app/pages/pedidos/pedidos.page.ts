import { UserStore } from './../../models/user_store';
import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.page.html',
  styleUrls: ['./pedidos.page.scss'],
})
export class PedidosPage implements OnInit {


  title = 'Productos';
  selectedSegment = 'recientes';
  userstore: UserStore = new UserStore();


  constructor() { }

  ngOnInit() {
    this.getUserStore();
  }

  segmentChanged(value) {
    this.selectedSegment = value;
  }

  getUserStore() {

    this.userstore = JSON.parse(localStorage.getItem('usuariostore'));
  }

}
