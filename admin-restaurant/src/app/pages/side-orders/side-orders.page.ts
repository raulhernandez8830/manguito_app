import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-side-orders',
  templateUrl: './side-orders.page.html',
  styleUrls: ['./side-orders.page.scss'],
})
export class SideOrdersPage implements OnInit {

  titulo = 'Side Orders';
  selectedSegment = 'listado';


  constructor() { }

  ngOnInit() {
  }

  segmentChanged(value) {
    this.selectedSegment = value;
  }

}
