import { FormControl, FormGroup } from '@angular/forms';
import { TipoSideOrder } from './../../../models/tipo_side_order';
import { SideOrder } from './../../../models/side_order';
import { NavParams, ModalController, ToastController, LoadingController } from '@ionic/angular';
import { SideorderService } from './../../../services/sideorder.service';
import { Component, OnInit } from '@angular/core';
import { UserStore } from 'src/app/models/user_store';

@Component({
  selector: 'app-detail-side-order',
  templateUrl: './detail-side-order.component.html',
  styleUrls: ['./detail-side-order.component.scss'],
})
export class DetailSideOrderComponent implements OnInit {

  sideorder: SideOrder;
  tipossideorders: TipoSideOrder[] = [];
  frm_editsideorder: FormGroup;

  constructor(private tiposideorders: SideorderService,
    private loadctr: LoadingController,
    private toastctr: ToastController,
     private modalctr: ModalController,
     private navparams: NavParams) {
    this.frm_editsideorder = new FormGroup({
      'nombre': new FormControl(''),
      'caracteristica': new FormControl('')
    });
  }

  ngOnInit() {
    this.tipossideorders = this.navparams.get('tipos');
    this.sideorder = this.navparams.get('sideorder');
    console.log(this.sideorder);

  }

  getTiposSideOrders() {
    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));
    this.tiposideorders.getTiposSideOrder(userstore).subscribe(
      response => {
        this.tipossideorders = response;
      }
    )
  }

  cancelaredicion() {
    this.modalctr.dismiss();
  }

  guardarEdicion() {
    this.sideorder.tipo_side_order = parseInt(
    (<HTMLInputElement>document.getElementById("tiposideorder")).value)
    this.presentLoading();
    console.log(this.sideorder);
    this.tiposideorders.editarSideOrder(this.sideorder).subscribe(
      response => {

      },
      err => {
        this.errorToast();
      },
      () => {
        this.successToast();
        this.cancelaredicion();
      }
    )
  }

  async successToast() {
    const toast = await this.toastctr.create({
      message: 'Side order editado con exito.',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async errorToast() {
    const toast = await this.toastctr.create({
      message: 'Error al guardar datos.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  // LOADER
  async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Guardando información...',
      duration: 2000,
      spinner: 'dots'
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }

}
