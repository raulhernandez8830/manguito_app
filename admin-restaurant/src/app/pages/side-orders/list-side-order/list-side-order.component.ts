import { TipoSideOrder } from './../../../models/tipo_side_order';
import { DetailSideOrderComponent } from './../detail-side-order/detail-side-order.component';
import { ModalController, ToastController, LoadingController } from '@ionic/angular';
import { SideOrder } from './../../../models/side_order';
import { SideorderService } from './../../../services/sideorder.service';
import { Component, OnInit } from '@angular/core';
import { UserStore } from 'src/app/models/user_store';

@Component({
  selector: 'app-list-side-order',
  templateUrl: './list-side-order.component.html',
  styleUrls: ['./list-side-order.component.scss'],
})
export class ListSideOrderComponent implements OnInit {

  sideorders: SideOrder[] = [];
  tipos: TipoSideOrder[] = [];
  texto: string;
  s = 1;

  constructor(private sideorderservice: SideorderService, private loadctr: LoadingController,
    private toastctr: ToastController,
    private modalctr: ModalController) { }

  ngOnInit() {
    this.getSideOrders();
    this.getTiposSideOrders();
  }

  getSideOrders() {
    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));
    this.sideorderservice.getSideOrders(userstore.store_id).subscribe(
      response => {
        this.sideorders = response;
      }
    )
  }




  async editarSideOrder(sideorder){
    console.log(this.tipos);
    const modal = await this.modalctr.create({
      component: DetailSideOrderComponent,
      componentProps: {
        'sideorder': sideorder,
        'tipos': this.tipos
      },
      cssClass: 'modaledicionproducto'
    });

    modal.onDidDismiss().then(
      () => {
        this.getSideOrders();
      }
    )

    return await modal.present();
  }

  getTiposSideOrders() {
    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));

    this.sideorderservice.getTiposSideOrder(userstore).subscribe(
      response => {
        this.tipos = response;
      }
    )
  }

  // LOADER
  async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Eliminando información...',
      duration: 2000,
      spinner: 'circular'
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }

  async errorToastDelete() {
    const toast = await this.toastctr.create({
      message: 'Error al elminar la informacion.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  async successDeleteToast() {
    const toast = await this.toastctr.create({
      message: 'Side order eliminado con exito.',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  deleteSideOrder(side){
    this.presentLoading();
    this.sideorderservice.deleteSideOrder(side.id).subscribe(
      response => {

      },
      err => {
        this.errorToastDelete();
      }, () => {
        this.successDeleteToast();
        this.getSideOrders();
      }
    )
  }

}
