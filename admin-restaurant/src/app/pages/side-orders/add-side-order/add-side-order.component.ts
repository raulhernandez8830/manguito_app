import { TipoSideOrder } from './../../../models/tipo_side_order';
import { SideorderService } from './../../../services/sideorder.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController, ToastController, LoadingController } from '@ionic/angular';
import { SideOrder } from 'src/app/models/side_order';
import { SideOrderDetail } from 'src/app/models/side_order_detail';
import { UserStore } from 'src/app/models/user_store';

@Component({
  selector: 'app-add-side-order',
  templateUrl: './add-side-order.component.html',
  styleUrls: ['./add-side-order.component.scss'],
})
export class AddSideOrderComponent implements OnInit {

  sideOrder: SideOrder[] = [];
  itemsList: SideOrder[] = [];
  tipos:any;
  tipo: any;
  precio:any;
  tamano:any;

  tipossideorder: TipoSideOrder[] = [];

  frm_sideorder: FormGroup;

  sideOrderDTO: SideOrder = new SideOrder();

  sideOrderDetail: SideOrderDetail[] = [];

  sideOrderDetailDTO: SideOrderDetail = new SideOrderDetail();

  constructor(private modalctr: ModalController, private loadctr: LoadingController,
    private toastctr: ToastController,
    private sideorderservice: SideorderService) {
    this.frm_sideorder = new FormGroup({
      'nombre': new FormControl('', Validators.required),
      'caracteristica': new FormControl('')
    });
  }

  ngOnInit() {

    this.tipos={
       "nombre":"Bebida",
       "value": true,
    }

    // obtener tipos de side order
    this.getTipoSideOrders();
  }

  // agregar elemento a sideOrderArray
  addElementToSideOrder(side: SideOrder) {
    this.sideOrder.push(side);
    this.sideOrderDTO = new SideOrder();
  }

  cancelarModal() {
    this.modalctr.dismiss();
  }

  addSideOrderDetail(sidedetail: SideOrderDetail){
    this.sideOrderDetail.push(sidedetail);
    this.sideOrderDetailDTO = new SideOrderDetail();
  }

  finalizarSideOrders() {

    this.itemsList.forEach(element => {
      this.sideorderservice.saveSideOrder(element).subscribe(
        response => {

        },
        err => {
          this.errorToast();
        },
        () => {
          this.successToast();
          this.itemsList.length = 0;
        }
      )
    });
  }

  // LOADER
  async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Cargando información...',
      duration: 2000,
      spinner: 'dots'
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }

  // metodos nuevos

  addItem(item: SideOrder){
    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));
    item.user_id = userstore.id;
    item.store_id = userstore.store_id;

    this.itemsList.push(item);
    this.sideOrderDTO = new SideOrder();
    console.log(this.itemsList);
  }

  deleteTask(index) {
    this.itemsList.splice(index, 1);
    }

  // obtener tipos de side order
  getTipoSideOrders() {
    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));

    this.sideorderservice.getTiposSideOrder(userstore).subscribe(
      response => {
        this.tipossideorder = response;
      }
    )
  }


  async successToast() {
    const toast = await this.toastctr.create({
      message: 'Side order agregado con exito.',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async errorToast() {
    const toast = await this.toastctr.create({
      message: 'Error al guardar datos.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

}
