import { PipesModule } from './../../pipes/pipes.module';
import { DetailSideOrderComponent } from './detail-side-order/detail-side-order.component';
import { AddSideOrderComponent } from './add-side-order/add-side-order.component';
import { ListSideOrderComponent } from './list-side-order/list-side-order.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SideOrdersPageRoutingModule } from './side-orders-routing.module';

import { SideOrdersPage } from './side-orders.page';
import { TipoSideOrderComponent } from './tipo-side-order/tipo-side-order.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    SideOrdersPageRoutingModule,
    ComponentsModule,
    NgxPaginationModule,
    PipesModule,
  ],
  declarations: [SideOrdersPage, ListSideOrderComponent, AddSideOrderComponent, TipoSideOrderComponent, DetailSideOrderComponent ]
})
export class SideOrdersPageModule {}
