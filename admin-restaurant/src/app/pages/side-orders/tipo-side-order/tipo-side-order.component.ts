import { SideorderService } from './../../../services/sideorder.service';
import { TipoSideOrder } from './../../../models/tipo_side_order';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormControlName, FormGroup, Validators } from '@angular/forms';
import { UserStore } from 'src/app/models/user_store';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-tipo-side-order',
  templateUrl: './tipo-side-order.component.html',
  styleUrls: ['./tipo-side-order.component.scss'],
})
export class TipoSideOrderComponent implements OnInit {

  frm_tiposideorder: FormGroup;
  tipossideorder: TipoSideOrder[] = [];

  constructor(private loadctr: LoadingController, private sideorderservice: SideorderService,
    private toastctr: ToastController) {
    this.frm_tiposideorder = new FormGroup({
      'nombre': new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
    this.getTiposSiderOrder();
  }

  // LOADER
  async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Guardando información...',
      duration: 2000,
      spinner: 'circular'
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }

  async successToast() {
    const toast = await this.toastctr.create({
      message: 'Tipo de side order agregado con exito.',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async successDeleteToast() {
    const toast = await this.toastctr.create({
      message: 'Tipo de side order eliminado con exito.',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async errorToast() {
    const toast = await this.toastctr.create({
      message: 'Error al guardar datos.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  async deleteErrorToast() {
    const toast = await this.toastctr.create({
      message: 'Error al eliminar datos.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  onSubmit() {
    this.presentLoading();
    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));

    let tiposiderorder: TipoSideOrder = new TipoSideOrder();
    tiposiderorder = this.frm_tiposideorder.value;
    tiposiderorder.store_id = userstore.store_id;
    tiposiderorder.user_id  = userstore.id;

    // llamada al service para guardar el tipo de side order
    this.sideorderservice.saveTipoSideOrder(tiposiderorder).subscribe(
      response => {},
      err => {
        this.errorToast();
      },
      () => {
        this.successToast();
        this.frm_tiposideorder.reset();
        this.tipossideorder.push(tiposiderorder);
      }
    )
  }

  // get tipos de side order
  getTiposSiderOrder() {
    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));
    this.sideorderservice.getTiposSideOrder(userstore).subscribe(
      response => {
        this.tipossideorder = response;
      },
      err =>{},
      () => {}
    )
  }

   delete(tiposideorder: TipoSideOrder, i) {
    Swal.fire({
      title: 'Estas seguro?',
      text: "",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'
    }).then((result) => {
      if (result.isConfirmed) {

        this.sideorderservice.deleteTipoSideOrder(tiposideorder).subscribe(
          response => {},
          err => {
            this.deleteErrorToast();
          },
          () => {
            this.successDeleteToast();
            this.tipossideorder.splice(i, 1);
          }
        )
        Swal.fire(
          'Eliminado!',
          '',
          'success'
        )
      }
    })
  }

}
