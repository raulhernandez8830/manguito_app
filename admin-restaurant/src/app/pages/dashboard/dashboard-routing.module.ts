import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardPage } from './dashboard.page';

const routes: Routes = [
  {
    path: '',
    component: DashboardPage,
    children: [
      {
        path: 'categories',
        loadChildren: () => import('../categories/categories.module').then( m => m.CategoriesPageModule)
      },

      {
        path: 'products',
        loadChildren: () => import('./../products/products.module').then( m => m.ProductsPageModule)
      },
      {
        path: 'pedidos',
        loadChildren: () => import('./../pedidos/pedidos.module').then( m => m.PedidosPageModule)
      },
      {
        path: 'side-orders',
        loadChildren: () => import('./../side-orders/side-orders.module').then( m => m.SideOrdersPageModule)
      },
      {
        path: 'tipos-menu',
        loadChildren: () => import('./../tipos-menu/tipos-menu.module').then( m => m.TiposMenuPageModule)
      },
      {
        path: 'deals',
        loadChildren: () => import('./../deals/deals.module').then( m => m.DealsPageModule)
      },
      {
        path: 'admin',
        loadChildren: () => import('./../admin/admin.module').then( m => m.AdminPageModule)
      },
      {
        path: 'home',
        loadChildren: () => import('./../home/home.module').then( m => m.HomePageModule)
      }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardPageRoutingModule {}
