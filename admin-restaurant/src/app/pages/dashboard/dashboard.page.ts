import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { UserStore } from 'src/app/models/user_store';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  titulo = 'Dashboard';
  userlogged: UserStore = new UserStore();

  constructor(private router: Router) { }

  ngOnInit() {
    this.getUserLogged();

  }



  getUserLogged() {
    // obtenemos el usuario para poder guardar la informacion de user/store
    this.userlogged = JSON.parse(localStorage.getItem('usuariostore'));
  }

}
