import { ToastController, LoadingController } from '@ionic/angular';
import { OpencloseService } from './../../../services/openclose.service';
import { Component, OnInit } from '@angular/core';
import { OpenClose } from 'src/app/models/open_close';
import { UserStore } from 'src/app/models/user_store';
import * as moment from 'moment';

@Component({
  selector: 'app-business-hours',
  templateUrl: './business-hours.component.html',
  styleUrls: ['./business-hours.component.scss'],
})
export class BusinessHoursComponent implements OnInit {

  openclose: OpenClose [] = [];
  openclosedto: OpenClose = new OpenClose();


  constructor(private opencloseservice: OpencloseService, private loadctr: LoadingController,
    private toastctr: ToastController) { }

  ngOnInit() {
    this.getHorariosForAdmin();
  }

  getHorariosForAdmin() {
     // obtenemos el usuario para poder guardar la informacion de user/store
     let userstore: UserStore = new UserStore();
     userstore = JSON.parse(localStorage.getItem('usuariostore'));
     this.opencloseservice.getHorariosForAdmin(userstore.store_id).subscribe(
       response => {
         this.openclose = response;
       }
     )
  }

  updateHorario(horario: OpenClose) {
    console.clear();
    console.log((<HTMLInputElement>document.getElementById("to"+horario.day)).value);
    this.openclosedto.store_id = horario.store_id;
    this.openclosedto.id = horario.id;
    this.openclosedto.to = moment((<HTMLInputElement>document.getElementById("to"+horario.day)).value, 'H:mm:ss').format('H:mm');
    this.openclosedto.from = moment((<HTMLInputElement>document.getElementById("from"+horario.day)).value, 'H:mm:ss').format('H:mm');
    this.openclosedto.day = horario.day;

    console.log(this.openclosedto);
    this.presentLoading();
    this.opencloseservice.updateHorario(this.openclosedto).subscribe(
      response => {

      },
      err => {
        this.errorToast();
      },
      () => {
        this.successToast();
      }
    )

  }

  async successToast() {
    const toast = await this.toastctr.create({
      message: 'Horario actualizado con exito.',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async errorToast() {
    const toast = await this.toastctr.create({
      message: 'Error al guardar datos.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  // LOADER
  async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Actualizando horario...',
      duration: 2000,
      spinner: 'bubbles'
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }

}
