import { BusinessHoursComponent } from './business-hours/business-hours.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { RecoveryPasswordComponent } from './recovery-password/recovery-password.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminPageRoutingModule } from './admin-routing.module';

import { AdminPage } from './admin.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdminPageRoutingModule,
    ComponentsModule,
    ReactiveFormsModule,
  ],
  declarations: [AdminPage, RecoveryPasswordComponent, ChangePasswordComponent, BusinessHoursComponent]
})
export class AdminPageModule {}
