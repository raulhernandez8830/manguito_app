import { ToastController, LoadingController } from '@ionic/angular';
import { UserService } from './../../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { UserStore } from 'src/app/models/user_store';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {

  confirmarpassword = false;
  pass1 = '';
  pass2 = '';

  constructor(private userservice: UserService, private toastctr: ToastController, private loadctr: LoadingController) { }

  ngOnInit() {}

  async successToast() {
    const toast = await this.toastctr.create({
      message: 'Password actualizado con exito.',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async errorToast() {
    const toast = await this.toastctr.create({
      message: 'Error al guardar datos.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  async warningpassword() {
    const toast = await this.toastctr.create({
      message: 'Las contraseñas digitadas no coinciden',
      duration: 2000,
      color: 'warning'
    });
    toast.present();
  }

  // LOADER
  async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Actualizando información...',
      duration: 2000,
      spinner: 'bubbles'
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }

  cambiarPassword() {
    this.presentLoading();
    if(this.pass1 === this.pass2) {

      let userstore: UserStore = new UserStore();
      userstore = JSON.parse(localStorage.getItem('usuariostore'));
      console.log(userstore);
      userstore.password = this.pass1;

      this.userservice.changePassword(userstore).subscribe(
        response => {

        },
        err => {
          this.errorToast();
        },
        () => {
          this.successToast();
        }
      )
    } else {
      this.warningpassword();
    }
  }

}
