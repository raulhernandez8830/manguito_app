import { SideOrder } from 'src/app/models/side_order';
import { UserStore } from './../models/user_store';
import { TipoSideOrder } from './../models/tipo_side_order';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SideorderService {

  url = environment.urlapi;

  constructor( private http: HttpClient) { }

  // guardar tipo de side order
  saveTipoSideOrder(tiposideorder: TipoSideOrder): Observable<TipoSideOrder> {
    return this.http.post(this.url + 'savetiposideorder', tiposideorder).pipe(
      map(data => data as TipoSideOrder )
    );
  }

  //get tipos de side order
  getTiposSideOrder(user: UserStore): Observable<TipoSideOrder[]> {
    return this.http.post(this.url + 'gettipossideorder', user).pipe(
      map(data => data as TipoSideOrder[])
    );
  }

  // eliminar un tipo de side order
  deleteTipoSideOrder(tiposideorder: TipoSideOrder): Observable<TipoSideOrder> {
    return this.http.post(this.url + 'deletetiposideorder', tiposideorder).pipe(
      map( data => data as TipoSideOrder)
    );
  }

  // guardar un side order
  saveSideOrder(sideorder: SideOrder): Observable<SideOrder> {
    return this.http.post(this.url + 'savesideorder', sideorder).pipe(
      map(data => data as SideOrder)
    );
  }

  // obtener los side ordes por tipo
  getSideOrdersByTipo(tipo, user): Observable<SideOrder[]> {
    var form = new FormData();
    form.append('user_id', user.id);
    form.append('store_id', user.store_id);
    form.append('tipo_side_order',tipo);
    return this.http.post(this.url + 'getsideorderbytipo', form).pipe(
      map(data => data as SideOrder[])
    );
  }

  // get side orders
  getSideOrders(store_id): Observable<SideOrder[]> {
    return this.http.get(this.url + 'getsideorders?store_id=' + store_id).pipe(
      map(data => data as SideOrder[])
    );
  }

  // editar un side order
  editarSideOrder(sideorder): Observable<SideOrder> {
    return this.http.post(this.url + 'editarSideOrder',sideorder).pipe(
      map(data => data as SideOrder)
    );
  }

  // obtener side orders por pedido
  getSideOrdersByPedido(pedido_id): Observable<any[]> {
    return this.http.get(this.url + 'getsidebypedido?pedido_id=' + pedido_id).pipe(
      map(data => data as any[])
    );
  }

  deleteSideOrder(id): Observable<SideOrder> {
    return this.http.get(this.url + 'deletesideorder?id=' + id).pipe(
      map(data => data as SideOrder)
    );
  }

  getSideOrderByProduct(product): Observable<SideOrder[]> {
    return this.http.get(this.url + 'getsideorderbymenu?id=' + product.id).pipe(
      map(data => data as SideOrder[])
    );
  }


  // delete side order de un menu
  deleteSideOrderToMenu(side,product): Observable<any> {
    return this.http.get(this.url + 'deletesideordertoproduct?product=' + product.id + '&side=' + side.id).pipe(
      map(data => data as any)
    );
  }

}
