import { SideOrderDTO } from './../models/side_order_dto';
import { DetallePedido } from './../models/detalle_pedido';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { map,filter } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { DetalleDTO } from '../models/detalle_dto';
import { SideOrder } from '../models/side_order';
import { Pedido } from '../models/pedido';

@Injectable({
  providedIn: 'root'
})
export class PedidosService {

  url = environment.urlapi;
  pedidos_init = new BehaviorSubject<any[]>([]);
  pedidos_init$ = this.pedidos_init.asObservable();
  conteo = new BehaviorSubject<number>(0);
  conteo$ = this.conteo.asObservable();

  detallepedido = new BehaviorSubject<DetallePedido[]>([]);
  detallepedido$ = this.detallepedido.asObservable();
  detallepedidofull = new BehaviorSubject<DetalleDTO[]>([]);
  detallepedidofull$ = this.detallepedidofull.asObservable();

  sideorders = new BehaviorSubject<SideOrder[]>([]);
  sideorders$ = this.sideorders.asObservable();

  pedidos = new BehaviorSubject<any[]>([]);
  pedidos$ = this.pedidos.asObservable();


  constructor(private http: HttpClient) { }

  // obtener pedidos recientes
  getPedidos(store):Observable<any[]> {
    return this.http.get(this.url + 'getpedidosforadmin?store_id='+ store).pipe(
      map(data => data as any[])
    );
  }

  getDetailPedido(pedido): Observable<any[]> {
    return this.http.get(this.url + 'getdetallepedidos?pedido_id=' + pedido.id).pipe(
      map(data => data as any[])
    );
  }

  actualizarEstadoPedido(pedido): Observable<any> {
    return this.http.post(this.url + 'updateestadopedido', pedido).pipe(
      map(data => data as any)
    );
  }

  pedidosIniciales(pedidos: any[]) {
    this.pedidos_init.next(pedidos);
  }

  getPedidosNuevos(store): Observable<any[]>{
    return this.http.get<any[]>(this.url + 'getpedidosforadmin?store_id='+ store).pipe(
      map(data => data.filter(p => p.estado_id === 1) )
      );
  }

  setConteo(conteo) {
    this.conteo.next(conteo);
  }

  setDetalle(detallepedido) {
    this.detallepedido.getValue().push(detallepedido);
  }


  //save pedido
  savePedido(pedido): Observable<Pedido> {
    return this.http.post(this.url + 'savepedido', pedido).pipe(
      map(data => data as Pedido)
    );
  }

  setDetalleFull(detalledto) {
    this.detallepedidofull.getValue().push(detalledto);
  }

  setSideOrder(side){
    this.sideorders.getValue().push(side);
  }

  deleteSide(data: SideOrder) {
    const sideArr: SideOrder[] = this.sideorders.getValue();

    sideArr.forEach((item, index) => {
      if (item === data) {sideArr.splice(index, 1); }
    });

    this.sideorders.next(sideArr);
  }

  resetDetallesPedido() {
    this.detallepedido.getValue().length = 0;
    this.detallepedidofull.getValue().length = 0;
  }

  resetSideOrder() {
    this.sideorders.getValue().length = 0;
  }

  setPedido(pedido: any) {
    this.pedidos.getValue().unshift(pedido);
  }

}
