import { Devices } from './../models/devices';
import { HttpClient } from '@angular/common/http';
import { GlobalService } from './global.service';
import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { Observable, BehaviorSubject } from 'rxjs';
import {map} from 'rxjs/operators';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  url = environment.urlapi;
  store_estado = new BehaviorSubject<String>('');
  store_estado$ = this.store_estado.asObservable();

  constructor(private globalservice: GlobalService, private http: HttpClient) { }

  // metodo para validar credenciales del usuario
  validarCredenciales(usuario: User): Observable<User> {
    return this.http.post<User>(this.url + 'validarcredenciales', usuario).pipe(
      map(data => data as User)
    );

  }

  // cambiar password
  changePassword(usuario): Observable<User>{
    return this.http.post(this.url + 'changepassword',  usuario).pipe(
      map(data => data as User)
    );
  }

  setEstadoTienda(estado,store_id): Observable<any> {
    return this.http.get(this.url + 'setestadotienda?estado=' + estado + '&store_id=' + store_id).pipe(
      map(data => data as any)
    );
  }

  getEstadoTienda(store_id): Observable<any> {
    return this.http.get(this.url + 'getestadotienda?store_id=' + store_id).pipe(
      map(data => data as any)
    );
  }

  setVariableEstadoTienda(estado) {
    this.store_estado.next(estado);
  }

  getDevices(store_id): Observable<Devices[]> {
    return this.http.get(this.url + 'getdevices?store_id=' + store_id).pipe(
      map(data => data as Devices[])
    );
  }

}
