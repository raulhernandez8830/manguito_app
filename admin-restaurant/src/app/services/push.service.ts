import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PushService {

  constructor(private http: HttpClient) { }

  // enviar notificacion push cuando el estado del pedido es actualizado
  pusNotificationhUpdateEstado(device, title, body): Observable<any> {
 // usar para manguito
    return this.http.get('https://us-central1-manguito-b66b6.cloudfunctions.net/manguitopushNewOrder?device=' + device + '&title=' + title + '&body=' + body).pipe(
       map(data => data as any)
    );

   // para fuego piante
   // return this.http.get('https://us-central1-manguito-b66b6.cloudfunctions.net/pushNewFuego?device=' + device + '&title=' + title + '&body=' + body).pipe(
   //   map(data => data as any)
   // );
  }


  // deals para fuego picante
  //https://us-central1-manguito-b66b6.cloudfunctions.net/pushManguitoDeals
  pushNotificationDealFuegoPicante(title, message, users): Observable<any> {
    return this.http.post('https://us-central1-manguito-b66b6.cloudfunctions.net/manguitopushDeals',{
      'title': title,
      'message': message,
      'users': users
    }).pipe(map(data => data as any));
  }

}


