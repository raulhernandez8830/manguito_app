import { TipoMenu } from './../models/tipo_menu';
import { SideOrder } from './../models/side_order';
import { environment } from './../../environments/environment';
import { UserStore } from 'src/app/models/user_store';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalService } from './global.service';
import { Injectable } from '@angular/core';
import { Product } from '../models/product';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  side = new BehaviorSubject<SideOrder[]>([]);
  side$ = this.side.asObservable();


  url = environment.urlapi;

  constructor(private globalservice: GlobalService, private http: HttpClient) { }

  //subir producto nuevo
  saveProduct(product: Product, userstore: UserStore, sideorders: SideOrder[]): Observable<Product> {
      var myFormData = new FormData();
      const headers = new HttpHeaders();
      headers.append('Content-Type', 'multipart/form-data');
      headers.append('Accept', 'application/json');
      myFormData.append('imagen', product.image);
      myFormData.append('precio', product.precio.toString());
      myFormData.append('nombre', product.nombre.toString());
      myFormData.append('descripcion', product.descripcion.toString());
      myFormData.append('categoria_id', product.categoria_id.toString());
      myFormData.append('user_id', userstore.id.toString());
      myFormData.append('store_id', userstore.store_id.toString());
      myFormData.append('tipo_menu', product.tipo_menu.toString());
      myFormData.append('sideorders', JSON.stringify(sideorders));
      myFormData.append('dia', product.dia);
      myFormData.append('always_available', product.always_available.toString());

    return this.http.post(this.url + 'saveproduct',myFormData,{
      headers: headers
      }).pipe(
      map(data => data as Product)
    );
  }


  // get products
  getProducts(userstore: UserStore): Observable<Product[]>
   {
     return this.http.post(this.url + 'getproducts', userstore).pipe(
       map(data => data as Product[])
     );
  }

  // editar la informacion de un producto
  editProduct(userstore: UserStore, product: Product, sideorders): Observable<Product>{

      var myFormData = new FormData();
      const headers = new HttpHeaders();
      headers.append('Content-Type', 'multipart/form-data');
      headers.append('Accept', 'application/json');
      myFormData.append('imagen', product.image);
      myFormData.append('precio', product.precio.toString());
      myFormData.append('nombre', product.nombre.toString());
      myFormData.append('descripcion', product.descripcion.toString());
      myFormData.append('categoria_id', product.categoria_id.toString());
      myFormData.append('user_id', userstore.id.toString());
      myFormData.append('store_id', userstore.store_id.toString());
      myFormData.append('estado', product.estado.toString());
      myFormData.append('precio_oferta', product.precio_oferta.toString());
      myFormData.append('tipo_menu', product.tipo_menu.toString());
      myFormData.append('id', product.id.toString());
      myFormData.append('sideorders', JSON.stringify(sideorders));
      myFormData.append('dia', product.dia);
      myFormData.append('always_available', JSON.stringify( product.always_available));


      return this.http.post(this.url + 'editproduct', myFormData).pipe(
        map(data => data as Product)
      );
  }

  // obtener tipos de menu
  getTiposMenu(userstore: UserStore): Observable<TipoMenu[]> {
    return this.http.post(this.url + 'gettiposmenu', userstore).pipe(
      map(data => data as TipoMenu[])
    );
  }

  addSideOrderBehavior(side: SideOrder) {
    this.side.next(this.side.getValue().concat(side));
  }

  // actualizar un tipo de menu
  updateTipoMenu(tipomenu: TipoMenu): Observable<TipoMenu> {
    return this.http.post(this.url + 'updatetipomenu', tipomenu).pipe(
      map(data => data as TipoMenu)
    );
  }

  // eliminar un tipo de menu
  deleteTipoMenu(tipomenu): Observable<TipoMenu> {
    return this.http.post(this.url + 'deletetipomenu', tipomenu).pipe(
      map(data => data as TipoMenu)
    );
  }

  // delete product
  deleteProduct(product): Observable<Product> {
    return this.http.get(this.url + 'deleteproduct?id=' + product.id).pipe(
      map(data => data as Product)
    );
  }


  deleteSide(data: SideOrder) {
    const sideArr: SideOrder[] = this.side.getValue();

    sideArr.forEach((item, index) => {
      if (item === data) {sideArr.splice(index, 1); }
    });

    this.side.next(sideArr);
  }

  limpiarSideOrder() {
    this.side.getValue().length = 0;
  }


}
