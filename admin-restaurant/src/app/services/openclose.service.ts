import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { OpenClose } from '../models/open_close';

@Injectable({
  providedIn: 'root'
})
export class OpencloseService {
  url = environment.urlapi;

  constructor(private http:HttpClient) { }

  // obtener los horarios de una tienda para el administrador
  getHorariosForAdmin(store_id): Observable<OpenClose[]> {
    return this.http.get(this.url + 'gethorariosforadmin?store_id=' + store_id).pipe(
      map(data => data as OpenClose[])
    );
  }

  // update open close
  updateHorario(openclose: OpenClose): Observable<OpenClose> {
    return this.http.post(this.url + 'updatehorarios', openclose).pipe(
      map(data => data as OpenClose)
    );
  }

}
