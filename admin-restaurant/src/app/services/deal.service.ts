import { Deal } from './../models/deal';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DealService {

  url = environment.urlapi;

  constructor(private http: HttpClient) { }

  // guardar un deal
  saveDeal(deal: Deal): Observable<Deal> {
    return this.http.post(this.url + 'savedeal',deal).pipe(
      map(data => data as Deal)
    );
  }

  //obtener los deals
  getDeals(store): Observable<any[]> {
    return this.http.get(this.url + 'getdeals?store_id=' + store).pipe(
      map(data => data as any[])
    );
  }

   // delete deals
   deleteDeals(deal): Observable<any> {
    return this.http.post(this.url + 'deletedeal', deal).pipe(
      map(data => data as any)
    );
  }

}
