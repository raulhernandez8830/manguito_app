import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { MenuController } from '@ionic/angular';
import { UserStore } from 'src/app/models/user_store';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

  userlogged: UserStore = new UserStore();


  constructor(private router: Router,
    private menu: MenuController,
    private http: HttpClient,
    private nativeAudio: NativeAudio
    ) {}

    ngOnInit() {


      this.getUserLogged();

    }

    verCategorias() {
      this.router.navigateByUrl('dashboard/categories');
      this.menu.close();
    }

    sonidoPushNot() {
      this.nativeAudio.preloadSimple('dashboard/alertapedido', '../../../assets/sonidos/Scoring a Point.wav');
    }

    products() {
      this.router.navigateByUrl('dashboard/products');
      this.menu.close();
    }

    verPedidos() {
      this.router.navigateByUrl('dashboard/pedidos');
      this.menu.close();
    }

    sideOrders() {
      this.router.navigateByUrl('dashboard/side-orders');
      this.menu.close();
    }

    tipomenu() {
      this.router.navigateByUrl('dashboard/tipos-menu');
      this.menu.close();
    }

    deals() {
      this.router.navigateByUrl('dashboard/deals');
      this.menu.close();
    }

    getUserLogged() {
      // obtenemos el usuario para poder guardar la informacion de user/store
      this.userlogged = JSON.parse(localStorage.getItem('usuariostore'));
    }

    getAdmin() {
      this.router.navigateByUrl('dashboard/admin');
      this.menu.close();
    }






}
