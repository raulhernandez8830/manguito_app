import { Router } from '@angular/router';
import { UserService } from './../../services/user.service';
import { UserStore } from 'src/app/models/user_store';
import { PedidosService } from './../../services/pedidos.service';
import { Component, Input, OnInit } from '@angular/core';
import { of, pipe } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';
import { ActionSheetController, LoadingController, ModalController, ToastController } from '@ionic/angular';
import { PushService } from 'src/app/services/push.service';
import { DetailsPedidoComponent } from 'src/app/pages/pedidos/details-pedido/details-pedido.component';
import { ModalAlertPedidoComponent } from 'src/app/pages/pedidos/list-pedidos/modal-alert-pedido/modal-alert-pedido.component';
import { PrintPedidoComponent } from 'src/app/pages/pedidos/print-pedido/print-pedido.component';
import { Insomnia } from '@ionic-native/insomnia/ngx';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  @Input() titulo: string;
  pedidos_: any[] = [];
  pedidos: any[] = [];
  pedidos_nuevos: any[] = [];
  conteo = 0;
  estado_tienda: string;
  isChecked = false;
  cerrado = false;
  abierto = false;

  texto: string;
  p: number = 1;
  audio = new Audio();
  reproducir = true;


  constructor(
    private pushservice: PushService,
    private toastctr: ToastController,
    private insomnia: Insomnia,
    private modalctr: ModalController,
    private pedidoservice: PedidosService, private router: Router,
    private loadctr: LoadingController,
    private userservice: UserService,
    private actionSheetController: ActionSheetController) { }

  ngOnInit() {
    this.getPedidos();

    this.getPedidosRecibidos();
    this.pedidoservice.conteo$.subscribe(
      response => {
        this.conteo = response;
      }
    );

    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));
    this.userservice.setVariableEstadoTienda(userstore.estado_tienda);

    this.userservice.store_estado$.subscribe(
      response => {
        this.estado_tienda = response.toString();
        console.log(response);
        if(response.toString() === 'Abierto') {
          this.isChecked = true;
          this.abierto = true;
        } else {
          this.isChecked = false;
          this.cerrado = true;
        }
      }
    );


    this.getOrderRealTime();

    // insomnia para que la pantalla no se apague
    this.insomnia.keepAwake();
  }

  getPedidosRecibidos() {
    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));
    this.pedidoservice.getPedidosNuevos(userstore.store_id).subscribe(
      response => {

        this.pedidoservice.setConteo(response.length);

      }
    )

  }

  getEstadoTienda() {
    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));
    this.userservice.getEstadoTienda(userstore.store_id).subscribe(
      response => {

      }
    )
  }

  // LOADER
  async cerrandoTienda() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Cerrando tienda...',
      duration: 3000,
      spinner: 'circles'
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }

  async abriendoTienda() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Abriendo tienda...',
      duration: 3000,
      spinner: 'circles'
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }


  cambio(e) {
    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));

    if(e.target.checked) {

      this.abriendoTienda();

      this.userservice.setEstadoTienda('Abierto', userstore.store_id).subscribe(
        response => {

        }, err => {

        },() => {
          this.cerrado = false;
          this.abierto = true;
          this.userservice.setVariableEstadoTienda('Abierto');
        }
      )

    } else {
      this.cerrandoTienda();
      this.userservice.setEstadoTienda('Cerrado', userstore.store_id).subscribe(
        response => {

        }, err => {

        },() => {
          this.cerrado = true;
          this.abierto = false;
          this.userservice.setVariableEstadoTienda('Cerrado');
        }
      )

    }
  }

  home() {
    this.router.navigateByUrl('dashboard/home');
  }







  // ------------------------------------------------------------------
    //CODIGO PARA QUE LOS PEDIDOS PUEDAN RECIBIRSE EN CUALQUIER MODULO
  //-------------------------------------------------------------------


  getPedidos() {
    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));
    this.pedidoservice.getPedidos(userstore.store_id).subscribe(
      response => {
        // hacemos la primera carga de los pedidos cuando inicia la aplicacion
        this.pedidoservice.pedidos.next(response);
      }
    )
  }

  async detalles(p) {
    const modal = await this.modalctr.create({
      component: DetailsPedidoComponent,
      cssClass: 'fullscreen',
      componentProps: {
        pedido: p
      }
    });

    return await modal.present();
  }


  aceptarPedido(pedido) {

    // obtenemos el usuario para poder guardar la informacion de user/store
    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));

    this.presentLoading();
    pedido.estado_id = 3;
    this.pedidoservice.actualizarEstadoPedido(pedido).subscribe(
      response => {

      },
      err => {

      },
      () => {
        this.successToast();
        this.getPedidos();

        // actualizamos el conteo de los pedidos recibidos que no han sido aceptados
        this.pedidoservice.getPedidosNuevos(userstore.store_id).subscribe(
          response => {
            this.pedidoservice.setConteo(response.length);
            console.log(response.length);

          }
        )

        // llamada al metodo de las push para enviar push al usuario
        let title = "Pedido aceptado";
        let body = "Su pedido fue aceptado por el restaurante y se notificara cuando este listo para ser entregado";
        this.pushservice.pusNotificationhUpdateEstado(pedido.userToken, title, body).subscribe(
          response => {

          },
          err => {

          },
          () => {

          }
        )

      }
    )
  }

  rechazarPedido(pedido) {
    this.presentLoading();
    pedido.estado_id = 7;
    this.pedidoservice.actualizarEstadoPedido(pedido).subscribe(
      response => {

      },
      err => {

      },
      () => {
        this.successToast();
        this.getPedidos();

         // llamada al metodo de las push para enviar push al usuario
         let title = "Pedido rechazado";
         let body = "Su pedido ha sido rechazado por el restaurante";
         this.pushservice.pusNotificationhUpdateEstado(pedido.userToken, title, body).subscribe(
           response => {

           },
           err => {

           },
           () => {

           }
         )
      }
    )
  }

  pedidoListo(pedido) {
    this.presentLoading();
    pedido.estado_id = 4;
    this.pedidoservice.actualizarEstadoPedido(pedido).subscribe(
      response => {

      },
      err => {

      },
      () => {
        this.successToast();
        this.getPedidos();

         // llamada al metodo de las push para enviar push al usuario
         let title = "Pedido Listo!!";
         let body = "Su pedido se encuentra listo para ser entregado";
         this.pushservice.pusNotificationhUpdateEstado(pedido.userToken, title, body).subscribe(
           response => {

           },
           err => {

           },
           () => {

           }
         )
      }
    )
  }

  async successToast() {
    const toast = await this.toastctr.create({
      message: 'Estado de pedido actualizado',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async errorToast() {
    const toast = await this.toastctr.create({
      message: 'Error al guardar datos.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  // LOADER
  async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Actualizando estado...',
      duration: 2000,
      spinner: 'bubbles'

    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }

  playAudio(){

    this.audio.src = "../../../../assets/sonidos/Scoring a Point.wav";
    this.audio.load();
    this.audio.play();
  }


  // modal para la notificacion
  async modalNotification() {
    const modal = await this.modalctr.create({
      component: ModalAlertPedidoComponent,

    });

    modal.onDidDismiss().then(() => {


    });

    return await modal.present();
  }


  getOrderRealTime() {
    setInterval(
      () => {

        // vamos a consultar los pedidos que tenemos actualmente
        this.pedidoservice.pedidos$.subscribe(
          response => {
            this.pedidos = response;
          }
        );

        // obtenemos el usuario para poder guardar la informacion de user/store
        let userstore: UserStore = new UserStore();
        userstore = JSON.parse(localStorage.getItem('usuariostore'));

        // realizamos una consulta para saber si tenemos nuevos pedidos en cola en la base de datos
        this.pedidoservice.getPedidos(userstore.store_id).subscribe(
          response => {

            this.pedidos_nuevos = response;
            console.log(this.pedidos_nuevos);

            //recorrido para comprobar si tenemos pedidos nuevos
            for(var i=0; i< this.pedidos_nuevos.length; i++) {
              if(this.pedidos.some(p => p.id === this.pedidos_nuevos[i].id)){
                console.log('no hay pedidos nuevos');

              } else {
                console.log('pedido nuevo encontrado');
                // llamada a la modal para mostrar que tenemos un nuevo pedido
                this.modalNotification();



                // agregamos el pedido nuevo a la carga inicial
                this.pedidoservice.setPedido(this.pedidos_nuevos[i]);

                // reseteamos los pedidos nuevos
                this.pedidos_nuevos.length = 0 ;

                // actualizamos el conteo de los pedidos recibidos que no han sido aceptados
                this.pedidoservice.getPedidosNuevos(userstore.store_id).subscribe(
                  response => {
                    this.pedidoservice.setConteo(response.length);
                    console.log(response.length);
                    this.pedidoservice.conteo$.subscribe();
                  }
                )
              }
            }
          }
        )

    },12000)
  }


  async imprimir(pedido) {
    const modal = await this.modalctr.create({
      component: PrintPedidoComponent,
      componentProps: {
        'pedido': pedido
      },
      cssClass: 'fullscreen',
    });

    return await modal.present();
  }

}
