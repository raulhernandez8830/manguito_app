import { Printer } from '@ionic-native/printer/ngx';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { NativeAudio } from '@ionic-native/native-audio/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComponentsModule } from './components/components.module';
import { SwiperModule } from 'swiper/angular';
import Swiper from 'swiper';
import {BluetoothSerial} from '@ionic-native/bluetooth-serial/ngx';
import { Insomnia } from '@ionic-native/insomnia/ngx';





@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule,

    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    ComponentsModule],

  providers: [
    NativeAudio,
    Printer,
    BluetoothSerial,
    Insomnia,

    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy, }
  ],

  bootstrap: [AppComponent],
})
export class AppModule {}
