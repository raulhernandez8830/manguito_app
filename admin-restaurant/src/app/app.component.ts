import { Component, isDevMode } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { UserStore } from './models/user_store';
import { HttpClient } from "@angular/common/http";
import { NativeAudio } from '@ionic-native/native-audio/ngx';


import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed,
} from '@capacitor/core';


const { PushNotifications } = Plugins;

const { App } = Plugins;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  userlogged: UserStore = new UserStore();

  constructor(private router: Router,
     private menu: MenuController,
     private http: HttpClient,
     private nativeAudio: NativeAudio
     ) {}

  ngOnInit() {
    if (isDevMode()) {
      console.log('👋 Development!');
    } else {
      console.log('💪 Production!');
    }

    this.getUserLogged();

  }

  verCategorias() {
    this.router.navigateByUrl('categories');
    this.menu.close();
  }

  sonidoPushNot() {
    this.nativeAudio.preloadSimple('alertapedido', '../../../assets/sonidos/Scoring a Point.wav');
  }

  products() {
    this.router.navigateByUrl('products');
    this.menu.close();
  }

  verPedidos() {
    this.router.navigateByUrl('pedidos');
    this.menu.close();
  }

  sideOrders() {
    this.router.navigateByUrl('side-orders');
    this.menu.close();
  }

  tipomenu() {
    this.router.navigateByUrl('tipos-menu');
    this.menu.close();
  }

  deals() {
    this.router.navigateByUrl('deals');
    this.menu.close();
  }

  getUserLogged() {
    // obtenemos el usuario para poder guardar la informacion de user/store
    this.userlogged = JSON.parse(localStorage.getItem('usuariostore'));
  }

  getAdmin() {
    this.router.navigateByUrl('admin');
    this.menu.close();
  }


  setTupPush(){
     // Request permission to use push notifications
    // iOS will prompt user and return if they granted permission or not
    // Android will just grant without prompting
    PushNotifications.requestPermission().then(result => {
      if (result.granted) {
        // Register with Apple / Google to receive push via APNS/FCM
        PushNotifications.register();
      } else {
        // Show some error
      }
    });

    PushNotifications.addListener(
      'registration',
      (token: PushNotificationToken) => {
       // alert('Push registration success, token: ' + token.value);
        localStorage.token=token.value;
        this.saveToken(token); //para guardar el token del usuario
         console.log('Push registration success, token: ' + token.value)
      },
    );

    PushNotifications.addListener('registrationError', (error: any) => {
      console.log('Error on registration: ' + JSON.stringify(error));
    });

    PushNotifications.addListener(
      'pushNotificationReceived',
      (notification: PushNotification) => {
        console.log('Push received: ' + JSON.stringify(notification));
        //llamar aca la funcion para el sonido personalizado
        this.nativeAudio.play('alertapedido');
      },
    );

    PushNotifications.addListener(
      'pushNotificationActionPerformed',
      (notification: PushNotificationActionPerformed) => {
      console.log('Push action performed: ' + JSON.stringify(notification));
      },
    );
  }

  saveToken(token){
    let user=this.userlogged.alias;
    let userId=1;// agrega a aca el id de user solo que no se si esta en userlogged
     let param='userId=1&store_id=1&devicePlatform=AndroidTgs&appType='+user+'&deviceToken='+token;
    this.http.post('https://tgsserver.com/El_Manguito/laravel_manguito/savepushnotification?'+param, {
    }).subscribe(
      data => {
        console.log(data)




      },
      error => {
        console.error('POST /account/exist', error);
      }
    );

    }


}

